//
//  transfer-pad.swift
//  Hoppi
//
//  Created by Paul Onions on 14/11/2020.
//  Copyright © 2020 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Cocoa

@objc class TransferPad : NSWindowController {
  override var windowNibName: String! { return "transfer-pad" }

  @IBOutlet var padWidget: NSTextView!
  
  override func windowDidLoad() {
    super.windowDidLoad()
    self.padWidget.font = NSFont(name: "Menlo", size: 14)  // make this a user preference
  }

  func clearPad() {
    self.padWidget.string = ""
  }

  func updateContents(text: String) {
    self.padWidget.string = text
  }
  
  func getContents() -> String {
    return self.padWidget.string
  }
}
