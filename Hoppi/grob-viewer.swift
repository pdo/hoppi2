//
//  grob-viewer.swift
//  Hoppi
//
//  Created by Paul Onions on 18/10/2020.
//  Copyright © 2020 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Cocoa

@objc class GrobViewer : NSWindowController {
  override var windowNibName: String! { return "grob-viewer" }

  @IBOutlet var grobInfoWidget: NSTextField!
  @IBOutlet var grobViewWidget: HoppiGrobView!
  @IBOutlet var grobScrollWidget: NSScrollView!
  @IBOutlet var scaleInfoWidget: NSTextField!
  @IBOutlet var scaleSliderWidget: NSSlider!
  
  var currentScale: Int = 200
  
  var width: Int = 0
  var height: Int = 0
  var pixels: [UInt8]? = nil

  override func windowDidLoad() {
    super.windowDidLoad()
  }

  @IBAction func scaleSliderChanged(_ sender: NSButton) {
    let scale = sender.intValue
    self.currentScale = Int(scale)
    self.scaleInfoWidget.stringValue = "\(scale)%"
    self.grobViewWidget.needsDisplay = true
  }
  
  func showGrob(width: Int, height: Int, pixels: [UInt8]) {
    self.width = width
    self.height = height
    self.pixels = pixels
    
    self.grobInfoWidget.stringValue = "\(width) by \(height)"
    self.grobViewWidget.draw(grobViewWidget.frame)
    self.grobViewWidget.needsDisplay = true
  }
}


// Custom NSView for drawing GROBs
//
@objc class HoppiGrobView : NSView {
  @IBOutlet var grobViewer: GrobViewer!

  override var acceptsFirstResponder: Bool { return true }
  
  override func draw(_ dirtyRect: NSRect) {
    super.draw(dirtyRect)
    
    let width = self.grobViewer.width
    let height = self.grobViewer.height
    let zoom = Double(self.grobViewer.currentScale)/100.0
    let bgW = zoom*Double(width)
    let bgH = zoom*Double(height)
    let dotW = zoom
    let dotH = zoom
    self.setFrameSize(NSSize(width: bgW, height: bgH))

    // Fill background
    NSColor.white.set()
    NSBezierPath.fill(NSRect(x: 0, y: 0, width: bgW, height: bgH))

    // Draw GROB dots
    if let pixels = self.grobViewer.pixels {
      NSColor.black.set()
      for y in 0..<height {
        for x in 0..<width {
          if pixels[y*width + x] == 1 {
            NSBezierPath.fill(NSRect(x: zoom*Double(x), y: zoom*Double(height-y-1), width: dotW, height: dotH))
          }
        }
      }
    }
  }

  @IBAction func copy(_ sender: NSObject) {
    let context = NSGraphicsContext.current
    let pboard = NSPasteboard.general
    let scale = self.grobViewer.currentScale / 50

    if let bitmap = NSBitmapImageRep.init(bitmapDataPlanes: nil, pixelsWide: scale*self.grobViewer.width/2, pixelsHigh: scale*self.grobViewer.height/2, bitsPerSample: 8, samplesPerPixel: 4, hasAlpha: true, isPlanar: false, colorSpaceName: NSColorSpaceName.calibratedRGB, bytesPerRow: 0, bitsPerPixel: 0) {
      let bitmapContext = NSGraphicsContext.init(bitmapImageRep: bitmap)
      NSGraphicsContext.current = bitmapContext
      self.draw(self.frame)
      pboard.declareTypes([.tiff], owner: self)
      pboard.setData(bitmap.tiffRepresentation, forType: .tiff)
      NSGraphicsContext.current = context
    }
  }
}
