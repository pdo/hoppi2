//
//  packet-logger.swift
//  Hoppi
//
//  Created by Paul Onions on 11/10/2020.
//  Copyright © 2020 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Cocoa

@objc class PacketLogger : NSWindowController {
  override var windowNibName: String! { return "packet-logger" }

  @IBOutlet var showHexWidget: NSButton!
  @IBOutlet var logTranscriptWidget: NSTextView!
  
  override func windowDidLoad() {
    super.windowDidLoad()
  }

  @IBAction func clearLog(_ sender: NSButton) {
    self.logTranscriptWidget.string = ""
  }

  func appendTxPacket(seq: UInt8, type: UInt8, data: [UInt8], chksum: [UInt8]) {
    let typeChar = Character(Unicode.Scalar(type))
    let dataChars = String(data.map {hpCodeToUnicodeChar($0)})
    let chkChars = String(chksum.map {hpCodeToUnicodeChar($0)})
    var log = "↩ \(seq) \(typeChar)〖\(dataChars)〗〖\(chkChars)〗\n"
    
    if self.showHexWidget.state == .on {
      let dataDigits = data.map {String(format: " %02X", $0)}
      let chkDigits = chksum.map {String(format: " %02X", $0)}
      let hexline = "      [\(dataDigits.joined()) ]  [\(chkDigits.joined()) ]\n"
      log += hexline
    }
      
    let txtstore = self.logTranscriptWidget.textStorage
    let n = txtstore?.length ?? 0
    txtstore?.replaceCharacters(in: NSRange(location: n, length: 0), with: log)
      
    let m = self.logTranscriptWidget.string.count
    self.logTranscriptWidget.scrollRangeToVisible(NSRange(location: m, length: 0))
    self.logTranscriptWidget.needsDisplay = true
  }
  
  func appendRxPacket(seq: UInt8, type: UInt8, data: [UInt8], chksum: [UInt8]) {
    let typeChar = Character(Unicode.Scalar(type))
    let dataChars = String(data.map {hpCodeToUnicodeChar($0)})
    let chkChars = String(chksum.map {hpCodeToUnicodeChar($0)})
    var log = "↪ \(seq) \(typeChar)〖\(dataChars)〗〖\(chkChars)〗\n"
    
    if self.showHexWidget.state == .on {
      let dataDigits = data.map {String(format: " %02X", $0)}
      let chkDigits = chksum.map {String(format: " %02X", $0)}
      let hexline = "      [\(dataDigits.joined()) ]  [\(chkDigits.joined()) ]\n"
      log += hexline
    }
      
    let txtstore = self.logTranscriptWidget.textStorage
    let n = txtstore?.length ?? 0
    txtstore?.replaceCharacters(in: NSRange(location: n, length: 0), with: log)
      
    let m = self.logTranscriptWidget.string.count
    self.logTranscriptWidget.scrollRangeToVisible(NSRange(location: m, length: 0))
    self.logTranscriptWidget.needsDisplay = true
  }
}
