//
//  kermit.swift
//  Hoppi
//
//  Created by Paul Onions on 14/07/2020.
//  Copyright © 2020 - 2022 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Foundation

//---------------------------------------------------------------------------//
// Basic Kermit character encode/decode functions
//
func tochar(_ byte: UInt8) -> UInt8 {byte &+ 32}
func unchar(_ char: UInt8) -> UInt8 {char &- 32}
func ctl(_ byte: UInt8) -> UInt8 {byte ^ 0x40}

//---------------------------------------------------------------------------//
// Calculator character codes
//
let irregularCodes:[UInt8: (String, Character)] = [
   31: ("",      "\u{2026}"),   // ellipsis              --> HORIZONTAL ELLIPSIS
  127: ("",      "\u{2592}"),   // medium shade          --> MEDIUM SHADE
  128: ("\\<)",  "\u{2220}"),   // angle                 --> ANGLE
  129: ("\\x-",  "\u{0101}"),   // x bar                 --> LATIN SMALL LETTER A WITH MACRON
  130: ("\\.V",  "\u{2207}"),   // gradient              --> NABLA
  131: ("\\v/",  "\u{221A}"),   // square root           --> SQUARE ROOT
  132: ("\\.S",  "\u{222B}"),   // integration           --> INTEGRAL
  133: ("\\GS",  "\u{03A3}"),   // Sigma                 --> GREEK CAPITAL LETTER SIGMA
  134: ("\\|>",  "\u{25B6}"),   // store                 --> BLACK RIGHT-POINTING TRIANGLE
  135: ("\\pi",  "\u{03C0}"),   // pi                    --> GREEK SMALL LETTER PI
  136: ("\\.d",  "\u{2202}"),   // derivation            --> PARTIAL DIFFERENTIAL
  137: ("\\<=",  "\u{2264}"),   // less than or equal    --> LESS-THAN OR EQUAL TO
  138: ("\\>=",  "\u{2265}"),   // greater than or equal --> GREATER-THAN OR EQUAL TO
  139: ("\\=/",  "\u{2260}"),   // not equal             --> NOT EQUAL TO
  140: ("\\Ga",  "\u{03B1}"),   // alpha                 --> GREEK SMALL LETTER ALPHA
  141: ("\\->",  "\u{2192}"),   // right arrow           --> RIGHTWARDS ARROW
  142: ("\\<-",  "\u{2190}"),   // left arrow            --> LEFTWARDS ARROW
  143: ("\\|v",  "\u{2193}"),   // down arrow            --> DOWNWARDS ARROW
  144: ("\\|^",  "\u{2191}"),   // up arrow              --> UPWARDS ARROW
  145: ("\\Gg",  "\u{03B3}"),   // gamma                 --> GREEK SMALL LETTER GAMMA
  146: ("\\Gd",  "\u{03B4}"),   // delta                 --> GREEK SMALL LETTER DELTA
  147: ("\\Ge",  "\u{03B5}"),   // epsilon               --> GREEK SMALL LETTER EPSILON
  148: ("\\Gn",  "\u{03B7}"),   // eta                   --> GREEK SMALL LETTER ETA
  149: ("\\Gh",  "\u{03B8}"),   // theta                 --> GREEK SMALL LETTER THETA
  150: ("\\Gl",  "\u{03BB}"),   // lambda                --> GREEK SMALL LETTER LAMBDA
  151: ("\\Gr",  "\u{03C1}"),   // rho                   --> GREEK SMALL LETTER RHO
  152: ("\\Gs",  "\u{03C3}"),   // sigma                 --> GREEK SMALL LETTER SIGMA
  153: ("\\Gt",  "\u{03C4}"),   // tau                   --> GREEK SMALL LETTER TAU
  154: ("\\Gw",  "\u{03C9}"),   // omega                 --> GREEK SMALL LETTER OMEGA
  155: ("\\GD",  "\u{0394}"),   // Delta                 --> GREEK CAPITAL LETTER DELTA
  156: ("\\PI",  "\u{03A0}"),   // Pi                    --> GREEK CAPTIAL LETTER PI
  157: ("\\GW",  "\u{03A9}"),   // Omega                 --> GREEK CAPTIAL LETTER OMEGA
  158: ("\\[]",  "\u{25A0}"),   // box                   --> BLACK SQUARE
  159: ("\\oo",  "\u{221E}"),   // infinity              --> INFINITY
  160: ("\\160", "\u{00A0}"),   // space                 --> NON-BREAKING SPACE
  171: ("\\<<",  "\u{00AB}"),   // begin program         --> LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
  176: ("\\^o",  "\u{00B0}"),   // degree                --> DEGREE SIGN
  181: ("\\Gm",  "\u{00B5}"),   // mu                    --> MICRO SIGN
  187: ("\\>>",  "\u{00BB}"),   // end program           --> RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
  215: ("\\.x",  "\u{00D7}"),   // multiplcation         --> MUTLIPLICATION SIGN
  216: ("\\O/",  "\u{00D8}"),   // O slash               --> LATIN CAPITAL LETTER O WITH STROKE
  223: ("\\Gb",  "\u{00DF}"),   // beta                  --> LATIN SMALL LETTER SHARP S
  247: ("\\:-",  "\u{00F7}"),   // division              --> DIVISION SIGN
]

let translatableCodes:[Character: UInt8] = [
  "\u{2018}": 39,   // LEFT SINGLE QUOTATION MARK   -->  APOSTROPHE
  "\u{2019}": 39,   // RIGHT SINGLE QUOTATION MARK  -->  APOSTROPHE
]

func unicodeCharToHpCode(_ char: Character) -> UInt8 {
  let irreg = irregularCodes.first{$1.1 == char}
  if irreg != nil {
    return irreg!.0
  }
  
  let trans = translatableCodes[char]
  if trans != nil {
    return trans!
  }
  
  return UInt8(char.asciiValue ?? Character(".").asciiValue!)
}

func hpCodeToUnicodeChar(_ code: UInt8) -> Character {
  let irreg = irregularCodes[code]
  return irreg?.1 ?? Character(Unicode.Scalar(code))
}

//---------------------------------------------------------------------------//
// Kermit data chunks
//

// Kermit Tx Quantum -- an indivisible sequence of data bytes to be
// embedded in a packet (cannot be split across packet boundaries)
struct KermitTxQuantum {
  let data: [UInt8]
  let numUserBytes: Int
  
  init(data: [UInt8], numUserBytes: Int) {
    self.data = data
    self.numUserBytes = numUserBytes
  }
}

// Kermit Tx Chunk -- a chunk of data suitable for creating a single packet
struct KermitTxChunk {
  let data: [UInt8]
  let numUserBytes: Int

  init(data: [UInt8], numUserBytes: Int) {
    self.data = data
    self.numUserBytes = numUserBytes
  }

  init(fromQuanta: [KermitTxQuantum]) {
    var data: [UInt8] = []
    var numUserBytes = 0
    
    for quantum in fromQuanta {
      data.append(contentsOf: quantum.data)
      numUserBytes += quantum.numUserBytes
    }
    self.init(data: data, numUserBytes: numUserBytes)
  }
}

//---------------------------------------------------------------------------//
// Kermit checksum functions
//
func compute1ByteChksum(seq: UInt8, type: UInt8, data: [UInt8]) -> [UInt8] {
  let len = data.count + 3
  var acc = tochar(UInt8(len)) &+ tochar(seq) &+ type
  for b in data {
    acc &+= b
  }
  let chksum = (acc &+ ((acc & 0xC0) >> 6)) & 0x3F
  return [tochar(chksum)]
}

func compute2ByteChksum(seq: UInt8, type: UInt8, data: [UInt8]) -> [UInt8] {
  let len = data.count + 4
  var acc = UInt16(tochar(UInt8(len))) &+ UInt16(tochar(seq)) &+ UInt16(type)
  for b in data {
    acc &+= UInt16(b)
  }
  let chksum = [UInt8((acc & 0x0FC0) >> 6), UInt8(acc & 0x003F)]
  return [tochar(chksum[0]), tochar(chksum[1])]
}

func compute3ByteChksum(seq: UInt8, type: UInt8, data: [UInt8]) -> [UInt8] {
  let len = data.count + 5
  var crcData = Array<UInt8>(repeating: 0, count: len-2)
  crcData[0] = tochar(UInt8(len))
  crcData[1] = tochar(seq)
  crcData[2] = type
  for n in 0..<data.count {
    crcData[n+3] = data[n]
  }
  var q: UInt32 = 0
  var crc: UInt32 = 0
  for c in crcData {
    q = (crc ^ UInt32(c)) & 0x0F
    crc = (crc >> 4) ^ (q &* 0x1081)
    q = (crc ^ (UInt32(c) >> 4)) & 0x0F
    crc = (crc >> 4) ^ (q &* 0x1081)
  }
  let chksum = [UInt8((crc >> 12) & 0x0F), UInt8((crc >> 6) & 0x3F), UInt8(crc & 0x3F)]
  return [tochar(chksum[0]), tochar(chksum[1]), tochar(chksum[2])]
}

func computeChecksum(seq: UInt8, type: UInt8, data: [UInt8], chkSize:Int) -> [UInt8] {
  switch chkSize {
  case 1 : return compute1ByteChksum(seq: seq, type: type, data: data)
  case 2 : return compute2ByteChksum(seq: seq, type: type, data: data)
  case 3 : return compute3ByteChksum(seq: seq, type: type, data: data)
  default: return []
  }
}

//---------------------------------------------------------------------------//
// Kermit packets
//
class KermitPacket {
  var seq: UInt8 = 0          // sequence number
  var type: UInt8 = 0         // packet type
  var data_chk: [UInt8] = []  // concatenated data & checksum

  // Create a packet from received data (including checksum bytes)
  init(seq: UInt8, type: UInt8, data_chk: [UInt8]) {
    self.seq = seq
    self.type = type
    self.data_chk = data_chk
  }

  // Create a packet to send, automatically appending checksum bytes
  init(seq: UInt8, type: UInt8, data: [UInt8], chkSize: Int = 0) {
    self.seq = seq
    self.type = type
    self.data_chk = data + computeChecksum(seq: seq, type: type, data: data, chkSize: chkSize)
  }

  // Extract the payload data bytes
  func data(chkSize: Int) -> [UInt8] {
    return Array<UInt8>(data_chk[0..<(self.data_chk.count - chkSize)])
  }

  // Extract the checksum bytes
  func chksum(chkSize: Int) -> [UInt8] {
    return Array<UInt8>(data_chk[(self.data_chk.count - chkSize)...])
  }

  // Verify the packet's checksum
  func verifyChecksum(chkSize: Int) -> Bool {
    let seq = self.seq
    let type = self.type
    let data = self.data(chkSize: chkSize)
    let chksum = self.chksum(chkSize: chkSize)
    let pass = chksum == computeChecksum(seq: seq, type: type, data: data, chkSize: chkSize)
    return pass
  }

  // Match packet against seq and/or type and/or subtype
  func matches(seq: UInt8? = nil, type: Character? = nil, subtype: Character? = nil) -> Bool {
    let type = type?.asciiValue
    let subtype = subtype?.asciiValue
    return (seq == nil || seq == self.seq) &&
           (type == nil || type == self.type) &&
           (subtype == nil || (self.data_chk.count >= 1 && self.data_chk[0] == subtype))
  }
}

struct PacketConsts {
  static let minSize = 2
  static let maxSize = 94
  static let soh = 1
}

//---------------------------------------------------------------------------//
// Kermit buffers
//
class KermitBuffer {
  unowned let protocolController: KermitProtocolController

  var buf = Array(repeating: UInt8(0), count: 1024)
  var rdPtr = 0
  var wrPtr = 0
  
  init(protocolController: KermitProtocolController) {
    self.protocolController = protocolController
  }

  func rdIncr(increment: Int = 1) {
    self.rdPtr = (self.rdPtr + increment) % self.buf.count
  }

  func wrIncr(increment: Int = 1) {
    self.wrPtr = (self.wrPtr + increment) % self.buf.count
  }

  func rdPeek(offset: Int = 0) -> UInt8 {
    self.buf[(self.rdPtr + offset) % self.buf.count]
  }
  
  func rdPop() -> UInt8 {
    let byte = self.rdPeek()
    self.rdIncr()
    return byte
  }

  func wrPush(byte: UInt8) {
    self.buf[self.wrPtr] = byte
    self.wrIncr()
  }
  
  func bufferLevel() -> Int {
    // Always assume wrPtr is ahead of rdPtr, accounting for modulo wrap
    if wrPtr >= rdPtr {
      return wrPtr - rdPtr
    } else {
      return (wrPtr + self.buf.count) - rdPtr
    }
  }
  
  func resetBuffer() {
    self.rdPtr = 0
    self.wrPtr = 0
  }
}

//
// Kermit Receive Buffer -- bytes in, packets out
//
class KermitRxBuffer : KermitBuffer {
  unowned let observer: KermitProtocolObserver?
  
  init(protocolController: KermitProtocolController, observer: KermitProtocolObserver? = nil) {
    self.observer = observer
    super.init(protocolController: protocolController)
  }
  
  func findStartOfPacket() -> Bool {
    while (self.buf[self.rdPtr] != UInt8(PacketConsts.soh)) && (self.rdPtr != self.wrPtr) {
      self.rdIncr()
    }
    return self.rdPeek() == UInt8(PacketConsts.soh)
  }
  
  func packetAvailable() -> Bool {
    if !self.findStartOfPacket() {
      return false
    }
    let lvl = self.bufferLevel()
    if lvl < 2 {
      return false
    }
    let pktlen = min(PacketConsts.maxSize, max(PacketConsts.minSize, Int(unchar(self.rdPeek(offset: 1)))))
    return lvl >= pktlen+2
  }
  
  func push(byte: UInt8) {
    self.wrPush(byte: byte)
  }
  
  func push(bytes: [UInt8]) {
    for b in bytes {
      self.wrPush(byte: b)
    }
  }
  
  func popPacket() -> KermitPacket? {
    if !self.packetAvailable() {
      return nil
    } else {
      let pktlen = min(PacketConsts.maxSize, max(PacketConsts.minSize, Int(unchar(self.rdPeek(offset: 1)))))
      let seq = unchar(self.rdPeek(offset: 2))
      let type = self.rdPeek(offset: 3)
      var data_chk = [UInt8](repeating: 0, count: pktlen-2)
      for b in 4..<(pktlen+2) {
        data_chk[b-4] = self.rdPeek(offset: b)
      }
      let pkt = KermitPacket(seq: seq, type: type, data_chk: data_chk)
      self.rdIncr(increment: pktlen+2)

      self.observer?.didPopPacket(pkt: pkt)
      
      return pkt
    }
  }
}

//
// Kermit Transmit Buffer -- packets in, bytes out
//
class KermitTxBuffer : KermitBuffer {
  unowned let observer: KermitProtocolObserver?
  
  init(protocolController: KermitProtocolController, observer: KermitProtocolObserver? = nil) {
    self.observer = observer
    super.init(protocolController: protocolController)
  }
  
  func pushPacket(pkt: KermitPacket) {
    let numPadBytes = self.protocolController.currentParameters.txNumPadBytes
    let padByte = self.protocolController.currentParameters.txPadByte
    let terminatorByte = self.protocolController.currentParameters.txPacketTerminatorByte
    if numPadBytes > 0 {
      for _ in 1...numPadBytes {
        self.wrPush(byte: padByte)
      }
    }
    self.wrPush(byte: UInt8(PacketConsts.soh))
    self.wrPush(byte: tochar(UInt8(pkt.data_chk.count+2)))
    self.wrPush(byte: tochar(UInt8(pkt.seq)))
    self.wrPush(byte: UInt8(pkt.type))
    for b in pkt.data_chk {
      self.wrPush(byte: b)
    }
    self.wrPush(byte: terminatorByte)
    self.observer?.didPushPacket(pkt: pkt)
  }

  func byteAvailable() -> Bool {
    return self.bufferLevel() > 0
  }
  
  func popByte() -> UInt8? {
    if !self.byteAvailable() {
      return nil
    }
    return self.rdPop()
  }
  
  func bytesAvailable() -> Int {
    return self.bufferLevel()
  }
  
  func popBytes(length: Int) -> [UInt8]? {
    if self.bytesAvailable() < length {
      return nil
    } else {
      var bytes = [UInt8](repeating: 0, count: length)
      for n in 0..<length {
        bytes[n] = self.rdPop()
      }
      return bytes
    }
  }
}

//---------------------------------------------------------------------------//
// Kermit protocol parameters
//
struct KermitProtocolParameters {
  var calculatorTimeoutPeriod = 3
  var hoppiTimeoutPeriod = 20
  var checksumSize = 1
  var binaryQuoteEnable = false
  var repeatPrefixEnable = false
  var binaryQuoteByte: UInt8 = Character("Y").asciiValue!
  var repeatPrefixByte: UInt8 = Character(" ").asciiValue!
  var txMaxPacketLength = 80
  var txNumPadBytes = 0
  var txPadByte: UInt8 = 0
  var txPacketTerminatorByte: UInt8 = 13
  var txCtrlQuoteByte: UInt8 = Character("#").asciiValue!
  var rxCtrlQuoteByte: UInt8 = Character("#").asciiValue!
}

enum KermitProtocolState {
  case receiveServerIdle
  case receiveInit
  case receiveFile
  case receiveData
  case sendInit
  case sendInitAck
  case openFile
  case sendFile
  case sendFileAck
  case sendData
  case sendDataAck
  case sendEOF
  case sendEOFAck
  case sendBreak
  case sendBreakAck
  case sendServerInit
  case sendServerInitAck
  case sendGenCmd
  case sendGenCmdAck
  case complete
  case abort
  case restart
  case restartNow
}

enum KermitProtocolTransition {
  case checksumFailure(nakSeq: UInt8? = nil)  // Rcvd packet checksum failed, optionally send NAK
  case bumpRetry(KermitProtocolState? = nil)  // Increment retry count, optionally transition to next state if possible
  case next(KermitProtocolState)              // Transition to next state
  case yield                                  // Pause state machine processing until another event happens (packet or tick)
}

//---------------------------------------------------------------------------//
// Kermit protocol controller
//
protocol KermitProtocolObserver : AnyObject {
  func didRunProtocol(state: KermitProtocolState, retryCount: Int)
  func didRunProtocolStep(state: KermitProtocolState)
  func didSetTxErrMsg(msg: String)
  func didSetRxErrMsg(msg: String)
  func didProcessSPacket()
  func didProcessIPacket()
  func didSendDPacket(numUserBytesSent: Int)
  func didPushPacket(pkt: KermitPacket)
  func didPopPacket(pkt: KermitPacket)
}

class KermitProtocolController {
  unowned let workspace: KermitWorkspace
  unowned let observer: KermitProtocolObserver?

  lazy var txBuffer: KermitTxBuffer = KermitTxBuffer(protocolController: self, observer: self.observer)
  lazy var rxBuffer: KermitRxBuffer = KermitRxBuffer(protocolController: self, observer: self.observer)

  var protocolState: KermitProtocolState = .receiveServerIdle
  var protocolErrMsg: String = ""
  var seqNum: UInt8 = 0
  var retryCount: Int = 0
  var retryLimit: Int = 5
  var timerCount: Int = 0
  var sendIdleNak: Bool = false
  var honourTimeoutPeriodRequest: Bool = false
  var currentParameters = KermitProtocolParameters()
  var pendingParameters = KermitProtocolParameters()
  var pendingTxChunks: [KermitTxChunk] = []
  var numUserBytesSent: Int = 0
  var nextTransferType: KWTransferType = .unknown

  var txErrMsg: String = "" {
    didSet { self.observer?.didSetTxErrMsg(msg: self.txErrMsg)}
  }
  
  var rxErrMsg: String = "" {
    didSet { self.observer?.didSetRxErrMsg(msg: self.rxErrMsg)}
  }

  init(workspace: KermitWorkspace, observer: KermitProtocolObserver? = nil) {
    self.workspace = workspace
    self.observer = observer
  }
  
  // Force the default state of the protocol controller
  func setDefaultState() {
    self.currentParameters = KermitProtocolParameters()
    self.pendingParameters = KermitProtocolParameters()
    
    self.protocolState = .receiveServerIdle
    self.protocolErrMsg = ""
    self.seqNum = 0
    self.retryCount = 0
    self.retryLimit = 5
    self.timerCount = 0
    
    self.honourTimeoutPeriodRequest = false
    self.pendingTxChunks = []
    self.numUserBytesSent = 0
    self.txErrMsg = ""
    self.rxErrMsg = ""
  }

  // Reset the protocol
  func resetProtocol() {
    self.txBuffer.resetBuffer()
    self.rxBuffer.resetBuffer()
    self.setDefaultState()
  }

  // Activate the pending parameters
  func activatePendingParameters() {
    self.currentParameters = self.pendingParameters
  }
  
  // Encode a byte value for transmission.
  // Taking care to perform control and binary prefixing.
  func encode(byte: UInt8) -> [UInt8] {
    let txCtrlQuoteByte = self.currentParameters.txCtrlQuoteByte
    let binaryQuoteEnable = self.currentParameters.binaryQuoteEnable
    let binaryQuoteByte = self.currentParameters.binaryQuoteByte
    let repeatPrefixEnable = self.currentParameters.repeatPrefixEnable
    let repeatPrefixByte = self.currentParameters.repeatPrefixByte

    if byte < 32 || byte == 127 {
      return [txCtrlQuoteByte, ctl(byte)]
    } else if byte < 128 {
      if byte == txCtrlQuoteByte {
        return [txCtrlQuoteByte, txCtrlQuoteByte]
      } else if binaryQuoteEnable && byte == binaryQuoteByte {
        return [txCtrlQuoteByte, binaryQuoteByte]
      } else if repeatPrefixEnable && byte == repeatPrefixByte {
        return [txCtrlQuoteByte, repeatPrefixByte]
      } else {
        return [byte]
      }
    } else if binaryQuoteEnable {
      if byte < 160 || byte == 255 {
        return [binaryQuoteByte, txCtrlQuoteByte, ctl(byte % 128)]
      } else if (byte % 128) == txCtrlQuoteByte {
        return [binaryQuoteByte, txCtrlQuoteByte, txCtrlQuoteByte]
      } else if (byte % 128) == binaryQuoteByte {
        return [binaryQuoteByte, txCtrlQuoteByte, binaryQuoteByte]
      } else if repeatPrefixEnable && (byte % 128) == repeatPrefixByte {
        return [binaryQuoteByte, txCtrlQuoteByte, repeatPrefixByte]
      } else {
        return [binaryQuoteByte, byte % 128]
      }
    } else {  // no binary quoting
      if byte < 160 || byte == 255 {
        return [txCtrlQuoteByte, ctl(byte)]
      } else {
        if (byte % 128) == txCtrlQuoteByte {
          return [txCtrlQuoteByte, byte]
        } else if repeatPrefixEnable && (byte % 128) == repeatPrefixByte {
          return [txCtrlQuoteByte, byte]
        } else {
          return [byte]
        }
      }
    }
  }
    
  // Encode a data byte to a KermitTxQuantum object
  func encodeItem(_ data: UInt8) -> KermitTxQuantum {
    return KermitTxQuantum(data: self.encode(byte: data), numUserBytes: 1)
  }
  
  // Encode a repeated data byte to an array of KermitTxQuantum objects
  func encodeRepeatedItem(_ data: UInt8, count: Int) -> [KermitTxQuantum] {
    let repeatPrefixByte = self.currentParameters.repeatPrefixByte
    var quantums: [KermitTxQuantum] = []
    var n = count
    while n > 0 {
      if count >= 2 {
        let qdata = [repeatPrefixByte, tochar(UInt8(min(n,94)))] + self.encodeItem(data).data
        let qnub = min(n,94)
        quantums.append(KermitTxQuantum(data: qdata, numUserBytes: qnub))
      } else {
        quantums.append(self.encodeItem(data))
      }
      n -= 94
    }
    return quantums
  }
  
  // Encode an array of data bytes to an array of KermitTxQuantum objects
  func encodeDataToQuanta(_ data: [UInt8]) -> [KermitTxQuantum] {
    let repeatPrefixEnable = self.currentParameters.repeatPrefixEnable
    if repeatPrefixEnable {
      var currentItem = data[0]
      var currentCount = 0
      var resultList: [KermitTxQuantum] = []
      for n in 0..<data.count {
        if data[n] == currentItem {
          currentCount += 1
        } else {
          resultList.append(contentsOf: self.encodeRepeatedItem(currentItem, count: currentCount))
          currentItem = data[n]
          currentCount = 1
        }
      }
      if currentCount > 0 {
        resultList.append(contentsOf: self.encodeRepeatedItem(currentItem, count: currentCount))
      }
      return resultList
    } else {
      return data.map(self.encodeItem)
    }
  }
  
  // Encode an array of data bytes to an array of KermitTxChunk objects
  func encodeDataToChunks(_ data: [UInt8]) -> [KermitTxChunk] {
    let checksumSize = self.currentParameters.checksumSize
    let txMaxPacketLength = self.currentParameters.txMaxPacketLength
    let quanta = self.encodeDataToQuanta(data)
    let maxDataLength = txMaxPacketLength - checksumSize - 2
    var currentLength = 0
    var currentQuanta: [KermitTxQuantum] = []
    var chunks: [KermitTxChunk] = []
    for quantum in quanta {
      let len = quantum.data.count
      if currentLength + len <= maxDataLength {
        currentQuanta.append(quantum)
        currentLength += len
      } else {
        chunks.append(KermitTxChunk(fromQuanta: currentQuanta))
        currentQuanta = [quantum]
        currentLength = len
      }
    }
    if currentLength > 0 {
      chunks.append(KermitTxChunk(fromQuanta: currentQuanta))
    }
    return chunks
  }
  
  // Encode an array of data bytes to an array of encoded data bytes
  func encode(data: [UInt8]) -> [UInt8] {
    let quanta = self.encodeDataToQuanta(data)
    let qdatas = quanta.map {$0.data}
    var encodedData: [UInt8] = []
    for qdata in qdatas {
      encodedData.append(contentsOf: qdata)
    }
    return encodedData
  }
  
  // Decode a fragment of data.
  // Accept a vector of bytes and an index into that vector. Return two
  // values: a decoded fragment of data (as a vector of byte values), and the index
  // to use to decode the next fragment.
  func decodeFragment(_ data: [UInt8], index: Int, allowRepeatPrefix: Bool = true) -> ([UInt8], Int) {
    let ctrlQuoteByte = self.currentParameters.rxCtrlQuoteByte
    let binaryQuoteEnable = self.currentParameters.binaryQuoteEnable
    let binaryQuoteByte = self.currentParameters.binaryQuoteByte
    let repeatPrefixEnable = self.currentParameters.repeatPrefixEnable
    let repeatPrefixByte = self.currentParameters.repeatPrefixByte
    let indexedByte = data[index]
    if indexedByte == ctrlQuoteByte {
      let nextByte = data[index+1]
      if nextByte % 128 == ctrlQuoteByte {
        return ([nextByte], index+2)
      } else if binaryQuoteEnable && (nextByte % 128 == binaryQuoteByte) {
        return ([nextByte], index+2)
      } else if repeatPrefixEnable && (nextByte % 128 == repeatPrefixByte) {
        return ([nextByte], index+2)
      } else {
        return ([ctl(nextByte)], index+2)
      }
    } else if binaryQuoteEnable && indexedByte == binaryQuoteByte {
      let (byts, idx) = self.decodeFragment(data, index: index+1, allowRepeatPrefix: false)
      return (byts.map {$0 | 0x80}, idx)
    } else if allowRepeatPrefix && repeatPrefixEnable && (indexedByte == repeatPrefixByte) {
      let rptCount = Int(unchar(data[index+1]))
      let (byts, idx) = self.decodeFragment(data, index: index+2, allowRepeatPrefix: false)
      return ([UInt8](repeating: byts[0], count: rptCount), idx)
    } else {
      return ([indexedByte], index+1)
    }
  }
  
  // Decode data from a received packet.
  // Accept a vector of byte values containing raw data from a received packet,
  // and return a vector of decoded byte values."
  func decode(data: [UInt8]) -> [UInt8] {
    var fragments: [[UInt8]] = []
    var index = 0
    while index < data.count {
      let (fragment, nextIndex) = self.decodeFragment(data, index: index)
      fragments.append(fragment)
      index = nextIndex
    }
    var decodedData: [UInt8] = []
    for fragment in fragments {
      decodedData.append(contentsOf: fragment)
    }
    return decodedData
  }

  // Routines to process flow from calculator to computer
  //
  // Process parameter data received in an S or I packet.
  func processParamRequest(data: [UInt8]) {
    self.pendingParameters = KermitProtocolParameters()
    if data.count >= 1 {
      self.pendingParameters.txMaxPacketLength = Int(unchar(data[0]))  // MAXL
    }
    if data.count >= 2 && self.honourTimeoutPeriodRequest {
      self.pendingParameters.hoppiTimeoutPeriod = Int(unchar(data[1]))  // TIME
    }
    if data.count >= 3 {
      self.pendingParameters.txNumPadBytes = Int(unchar(data[2]))  // NPAD
    }
    if data.count >= 4 {
      self.pendingParameters.txPadByte = ctl(data[3])  // PADC
    }
    if data.count >= 5 {
      self.pendingParameters.txPacketTerminatorByte = unchar(data[4])  // EOL
    }
    if data.count >= 6 {
      self.pendingParameters.rxCtrlQuoteByte = data[5]  // QCTL
    }
    if data.count >= 7 {
      self.pendingParameters.binaryQuoteByte = data[6]  // QBIN
    }
    if data.count >= 8 {
      self.pendingParameters.checksumSize = Int(data[7]) - Int(Character("0").asciiValue!)  // CHKT
    }
    if data.count >= 9 {
      self.pendingParameters.repeatPrefixByte = data[8]  // REPT
    }

    let binaryQuoteByte = self.pendingParameters.binaryQuoteByte
    if binaryQuoteByte == Character("Y").asciiValue! ||
      binaryQuoteByte == Character("N").asciiValue! ||
      binaryQuoteByte < 33 ||
      (binaryQuoteByte > 62 && binaryQuoteByte < 96) ||
      binaryQuoteByte > 126 {
      self.pendingParameters.binaryQuoteEnable = false
    } else {
      self.pendingParameters.binaryQuoteEnable = true
    }
    
    if self.pendingParameters.repeatPrefixByte == Character(" ").asciiValue! {
      self.pendingParameters.repeatPrefixEnable = false
    } else {
      self.pendingParameters.repeatPrefixEnable = true
    }
  }
    
  // Process an 'I' packet -- initialisation
  func process_I_packet(_ pkt: KermitPacket) {
    processParamRequest(data: pkt.data(chkSize: self.currentParameters.checksumSize))
    self.observer?.didProcessIPacket()
  }

  // Process an 'S' packet -- send initialisation
  func process_S_packet(_ pkt: KermitPacket) {
    self.processParamRequest(data: pkt.data(chkSize: self.currentParameters.checksumSize))
    self.observer?.didProcessSPacket()
  }
  
  // Construct parameter data to send in an ACK response to an S or I packet
  func constructParamResponse() -> [UInt8] {
    let pending = self.pendingParameters
    return [
      tochar(UInt8(PacketConsts.maxSize)),  //MAXL
      tochar(UInt8(pending.calculatorTimeoutPeriod)),  // TIME
      tochar(0),  // NPAD
      ctl(0),  // PADC
      tochar(13),  // EOL
      pending.txCtrlQuoteByte,  // QCTL
      pending.binaryQuoteByte,  // QBIN
      UInt8(pending.checksumSize + Int(Character("0").asciiValue!)),  // CHKT
      pending.repeatPrefixByte  // REPT
    ]
  }
  
  // Send an ACK packet with initialisation parameters
  func sendAckWithParams(seqNum: UInt8? = nil) {
    let seq = seqNum ?? self.seqNum
    let type = Character("Y").asciiValue!
    let chkSize = self.currentParameters.checksumSize
    let data = constructParamResponse()
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }
    
  // Process a 'C' packet -- host command.
  // On failure of the command return nil and an error message string, otherwise
  // return a title string and a response string to send back to the calculator,
  // or nil and nil if there is nothing to send back.
  func process_C_packet(_ pkt: KermitPacket) -> (String?, String?) {
    //let decodedData = self.decodeData(pkt.data(chkSize: self.currentParameters.checksumSize))
    //let command = String(decodedData.map(hpCodeToUnicodeChar))
    // !!! TODO -- check command and dispatch appropriately !!!
    // !!! for now just assume it's 'DIR /W' from an HP48 !!!
    let ws = self.workspace
    let (listing, dirpath) = ws.listFolderForReading(ws.currentFolderPath(), style: .DOS)
    if listing == nil {
      return (nil, "Cannot list directory: \(dirpath ?? "''")")
    } else {
      return ("File listing", listing!)
    }
  }
  
  // Decode and extract a generic command's parameters
  func extractGenericCommandParameters(data: [UInt8]) -> [String] {
    var params: [String] = []
    var base = 0
    while base < data.count {
      let len = Int(unchar(data[base]))
      if len > 0 && base+len < data.count {
        let pdata = data[(base+1)...(base+len)]
        let pchars = pdata.map(hpCodeToUnicodeChar)
        params.append(String(pchars))
      }
      base += len+1
    }
    return params
  }
  
  // Process a 'G' packet -- generic Kermit command.
  // On failure of the command return nil and an error message string, otherwise
  // return a title string and a response string to send back to the calculator,
  // or nil and nil if there is nothing to send back.
  func process_G_packet(_ pkt: KermitPacket) -> (String?, String?) {
    let ws = self.workspace
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    let cmd = data.count >= 1 ? Character(Unicode.Scalar(data[0])) : Character("-")
    switch cmd {
    case "C":  // Change directory
      let params = extractGenericCommandParameters(data: Array(data[1...]))
      let dirname = params.first ?? ws.homeFolderPath()
      let (success, msg) = ws.updateCurrentFolderPath(dirname)
      if success {
        return ("Current directory", msg)
      } else {
        return (nil, msg)
      }
    case "D":  // List directory contents
      let params = extractGenericCommandParameters(data: Array(data[1...]))
      let dirname = params.first ?? ws.homeFolderPath()
      let (listing, dirpath) = ws.listFolderForReading(dirname, style: .RPL)
      if listing == nil {
        return (nil, "Cannot list directory: \(dirpath ?? "''")")
      } else {
        return ("File listing", listing)
      }
    case "F":  // Finish command (ignore it)
      return (nil, nil)
    case "V":  // Variable set/get -- currently very limited
      let params = extractGenericCommandParameters(data: Array(data[1...]))
      let cmd = params.count >= 1 ? params[0] : nil
      let name = params.count >= 2 ? params[1] : nil
      let value = params.count >= 3 ? params[2] : nil
      if cmd?.uppercased() == "S" && name?.uppercased() == "ALLOW-OVERWRITE" && value?.uppercased() == "ON" {
        // set allow-overwrite on
        ws.allowOverwrite = true
        return ("Var ALLOW-OVERWRITE", "ON")
      } else if cmd?.uppercased() == "S" && name?.uppercased() == "ALLOW-OVERWRITE" && value?.uppercased() == "OFF" {
        // set allow-overwrite off
        ws.allowOverwrite = false
        return ("Var ALLOW-OVERWRITE", "OFF")
      } else if cmd?.uppercased() == "Q" && name?.uppercased() == "ALLOW-OVERWRITE" {
        // query allow-overwrite status
        return ("Var ALLOW-OVERWRITE", ws.allowOverwrite ? "ON" : "OFF")
      } else if cmd?.uppercased() == "S" && name?.uppercased() == "TRANSFER-TYPE" {
        // set next transfer-type
        if value?.uppercased() == "TEXT-AS-STRING" {
          self.nextTransferType = .textAsString
          return ("Var TRANSFER-TYPE", "TEXT-AS-STRING")
        } else if value?.uppercased() == "TEXT" {
          self.nextTransferType = .text
          return ("Var TRANSFER-TYPE", "TEXT")
        } else if value?.uppercased() == "BINARY" {
          self.nextTransferType = .binary
          return ("Var TRANSFER-TYPE", "BINARY")
        } else {
          self.nextTransferType = .unknown
          return ("Var TRANSFER-TYPE", "UNKNOWN")
        }
      } else if cmd?.uppercased() == "Q" && name?.uppercased() == "TRANSFER-TYPE" {
        // query next transfer-type status
        switch self.nextTransferType {
        case .textAsString:
          return ("Var TRANSFER-TYPE", "TEXT-AS-STRING")
        case .text:
          return ("Var TRANSFER-TYPE", "TEXT")
        case .binary:
          return ("Var TRANSFER-TYPE", "BINARY")
        case .unknown:
          return ("Var TRANSFER-TYPE", "UNKNOWN")
        }
      } else {
        return (nil, "Unknown var/cmd")
      }
    default:
      return (nil, "Unsupported command")
    }
  }
  
  // Process an 'R' packet -- receive initialization.
  // Return true if the requested file exists and can be opened for reading,
  // otherwise return false.  Also return a string as secondary result: on
  // success return the name of the file opened, and on failure return an
  // error message.
  func process_R_packet(_ pkt: KermitPacket) -> (Bool, String) {
    let ws = self.workspace
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    let filename = String(data.map(hpCodeToUnicodeChar))
    let transferType = self.nextTransferType
    self.nextTransferType = .unknown
    if filename.count == 0 {
      return ws.openPadForReading(transferType: transferType)
    } else {
      return ws.openFileForReading(filename, transferType: transferType)
    }
  }
  
  // Process an 'F' packet -- file header.
  // Return true if the requested file can be opened for writing, otherwise false.
  // Also return a string as secondary result: on success return the name of
  // the file opened (which may be different to that requested, e.g. with a
  // suffix to prevent overwriting a file), and on failure return an error
  // message.
  func process_F_packet(_ pkt: KermitPacket) -> (Bool, String) {
    let ws = self.workspace
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    let filename = String(data.map(hpCodeToUnicodeChar))
    let transferType = self.nextTransferType
    self.nextTransferType = .unknown
    if filename.count == 0 {
      return ws.openPadForWriting(transferType: transferType)
    } else {
      return ws.openFileForWriting(filename, transferType: transferType)
    }
  }
  
  // Process an 'X' packet -- message header.
  // Return true if the requested message title can be handled, otherwise false.
  // Also return a string as secondary result: on success return the message
  // title and on failure return an error message.
  func process_X_packet(_ pkt: KermitPacket) -> (Bool, String) {
    let ws = self.workspace
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    let title = String(data.map(hpCodeToUnicodeChar))
    let transferType = self.nextTransferType
    self.nextTransferType = .unknown
    return ws.openMessageForWriting(title: title, transferType: transferType)
  }

  // Process a 'D' packet -- data packet.
  // Return true if data was written successfully, otherwise return false.
  // Also return a string as secondary result: on success return the path of
  // the file to which we are writing, and on failure return an error message."
  func process_D_packet(_ pkt: KermitPacket) -> (Bool, String) {
    let ws = self.workspace
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    return ws.writeToCurrentOutputStream(data: data)
  }
  
  // Process a 'Z' packet -- end of file.
  func process_Z_packet(_ pkt: KermitPacket) {
    let ws = self.workspace
    ws.closeCurrentOutputStream()
  }
  
  // Process an 'E' packet -- error indication.
  func process_E_packet(_ pkt: KermitPacket) {
    let chkSize = self.currentParameters.checksumSize
    let data = self.decode(data: pkt.data(chkSize: chkSize))
    let msg = String(data.map(hpCodeToUnicodeChar))
    self.rxErrMsg = msg
  }

  // Send an ACK packet
  func sendAck(seqNum: UInt8? = nil) {
    let seq = seqNum ?? self.seqNum
    let type = Character("Y").asciiValue!
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: [], chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }
  
  // Send a NAK packet
  func sendNak(seqNum: UInt8? = nil) {
    let seq = seqNum ?? self.seqNum
    let type = Character("N").asciiValue!
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: [], chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }
  
  // Send an error packet with the given message
  func send_E_packet(message: String) {
    self.txErrMsg = message
    let seq = self.seqNum
    let type = Character("E").asciiValue!
    let chunks = self.encodeDataToChunks(message.map(unicodeCharToHpCode))
    let data = chunks.first?.data ?? []
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // Routines to process flow from calculator to computer
  //
  // Construct parameter data to send in an S or I packet
  func constructParamRequest() -> [UInt8] {
    self.pendingParameters = KermitProtocolParameters()
    self.pendingParameters.txMaxPacketLength = PacketConsts.maxSize
    self.pendingParameters.checksumSize = 3
    return [
      tochar(UInt8(self.pendingParameters.txMaxPacketLength)),                 // MAXL
      tochar(UInt8(self.pendingParameters.calculatorTimeoutPeriod)),           // TIME
      tochar(UInt8(self.pendingParameters.txNumPadBytes)),                     // NPAD
      ctl(self.pendingParameters.txPadByte),                                   // PADC
      tochar(self.pendingParameters.txPacketTerminatorByte),                   // EOL
      self.pendingParameters.txCtrlQuoteByte,                                  // QCTL
      self.pendingParameters.binaryQuoteByte,                                  // QBIN
      UInt8(self.pendingParameters.checksumSize) + Character("0").asciiValue!, // CHKT
      self.pendingParameters.repeatPrefixByte,                                 // REPT
    ]
  }

  // Send a send-initiation packet
  func send_S_packet(seqNum: UInt8? = nil, chkSize: Int? = nil) {
    let seq = seqNum ?? self.seqNum
    let type = Character("S").asciiValue!
    let data = self.constructParamRequest()
    let chkSize = chkSize ?? self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // Send an initialize packet
  func send_I_packet(seqNum: UInt8? = nil, chkSize: Int? = nil) {
    let seq = seqNum ?? self.seqNum
    let type = Character("I").asciiValue!
    let data = self.constructParamRequest()
    let chkSize = chkSize ?? self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // Process parameter data received in an ACK response to an S or I packet
  func processParamResponse(data: [UInt8]) {
    if data.count >= 1 {
      self.pendingParameters.txMaxPacketLength = Int(unchar(data[0]))  // MAXL
    }
    if data.count >= 2 && self.honourTimeoutPeriodRequest {
      self.pendingParameters.hoppiTimeoutPeriod = Int(unchar(data[1]))  // TIME
    }
    if data.count >= 3 {
      self.pendingParameters.txNumPadBytes = Int(unchar(data[2]))  // NPAD
    }
    if data.count >= 4 {
      self.pendingParameters.txPadByte = ctl(data[3])  // PADC
    }
    if data.count >= 5 {
      self.pendingParameters.txPacketTerminatorByte = unchar(data[4])  // EOL
    }
    if data.count >= 6 {
      self.pendingParameters.rxCtrlQuoteByte = data[5]  // QCTL
    }
    if data.count >= 7 {
      self.pendingParameters.binaryQuoteByte = data[6]  // QBIN
    }
    if data.count >= 8 {
      self.pendingParameters.checksumSize = Int(data[7] - Character("0").asciiValue!)  // CHKT
    }
    if data.count >= 9 {
      self.pendingParameters.repeatPrefixByte = data[8]  // REPT
    }
    
    let bqb = self.pendingParameters.binaryQuoteByte
    if bqb == Character("Y").asciiValue! || bqb == Character("N").asciiValue! ||
      bqb < 33 || (bqb > 62 && bqb < 96) || bqb > 126 {
      self.pendingParameters.binaryQuoteEnable = false
    } else {
      self.pendingParameters.binaryQuoteEnable = true
    }
    
    if self.pendingParameters.repeatPrefixByte == Character(" ").asciiValue! {
      self.pendingParameters.repeatPrefixEnable = false
    } else {
      self.pendingParameters.repeatPrefixEnable = true
    }

    self.activatePendingParameters()
  }

  // Process a 'Y' packet received in acknowledgement to an 'S' or 'I' packet
  func processAckWithParams(pkt: KermitPacket) {
    self.processParamResponse(data: pkt.data(chkSize: self.currentParameters.checksumSize))
  }
  
  // Send a file header packet
  func send_F_packet() {
    let seq = self.seqNum
    let type = Character("F").asciiValue!
    let name = self.workspace.inputUrl?.lastPathComponent ?? ""
    let chunks = self.encodeDataToChunks(name.map(unicodeCharToHpCode))
    let data = chunks.first?.data ?? []
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // Send a text header packet
  func send_X_packet() {
    let seq = self.seqNum
    let type = Character("X").asciiValue!
    let name = self.workspace.inputUrl?.lastPathComponent ?? ""
    let chunks = self.encodeDataToChunks(name.map(unicodeCharToHpCode))
    let data = chunks.first?.data ?? []
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // Send data packet
  func send_D_packet() {
    if let chunk = self.pendingTxChunks.first {
      self.pendingTxChunks.removeFirst()
      self.numUserBytesSent += chunk.numUserBytes
      let seq = self.seqNum
      let type = Character("D").asciiValue!
      let data = chunk.data
      let chkSize = self.currentParameters.checksumSize
      let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
      self.txBuffer.pushPacket(pkt: packet)
    }
    self.observer?.didSendDPacket(numUserBytesSent: self.numUserBytesSent)
  }
  
  // Send end-of-file packet
  func send_Z_packet() {
    let seq = self.seqNum
    let type = Character("Z").asciiValue!
    let data: [UInt8] = []
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }
  
  // Send break packet
  func send_B_packet() {
    let seq = self.seqNum
    let type = Character("B").asciiValue!
    let data: [UInt8] = []
    let chkSize = self.currentParameters.checksumSize
    let packet = KermitPacket(seq: seq, type: type, data: data, chkSize: chkSize)
    self.txBuffer.pushPacket(pkt: packet)
  }

  // The Kermit protocol state machine
  //
  // Increment sequence number mod 64 and set retry count to zero
  func incrSeq() {
    self.seqNum = (self.seqNum + 1) % 64
    self.retryCount = 0
  }
  
  // Run the Kermit protocol state machine one step
  func runProtocolStep(packet: KermitPacket? = nil, tick: Bool = false) -> KermitProtocolTransition {
    let hoppiTimeoutPeriod = self.currentParameters.hoppiTimeoutPeriod
    let checksumSize = self.currentParameters.checksumSize

    if tick {
      self.timerCount += 1
    }

    if packet != nil {
      self.timerCount = 0
    }

    switch self.protocolState {
    
    // RECEIVE-SERVER-IDLE -- server idle, waiting for a message
    //
    case .receiveServerIdle:
      self.seqNum = 0
      self.retryCount = 0

      // I packet
      if packet?.matches(seq: 0, type: "I") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_I_packet(packet!)
          self.sendAckWithParams(seqNum: 0)
          return .yield
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // S packet
      } else if packet?.matches(seq: 0, type: "S") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_S_packet(packet!)
          self.sendAckWithParams(seqNum: 0)
          self.incrSeq()
          return .next(.receiveFile)
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // R packet
      } else if packet?.matches(seq: 0, type: "R") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          let (success, msg) = self.process_R_packet(packet!)
          if success {
            return .next(.sendInit)
          } else {
            self.send_E_packet(message: msg)
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // K packet
      } else if packet?.matches(seq: 0, type: "K") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "K not implemented")
          return .yield
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // C packet
      } else if packet?.matches(seq: 0, type: "C") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          let (success, response) = self.process_C_packet(packet!)
          if let succ = success {
            if let resp = response {
              self.workspace.openMessageForReading(title: succ, msg: resp, transferType: .text)
              return .next(.sendInit)
            } else {
              self.sendAck()
              self.incrSeq()
              return .next(.complete)
            }
          } else {
            self.send_E_packet(message: response ?? "")
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // G packet
      } else if packet?.matches(seq: 0, type: "G") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          let (success, response) = self.process_G_packet(packet!)
          if let succ = success {
            if let resp = response {
              self.workspace.openMessageForReading(title: succ, msg: resp, transferType: .text)
              return .next(.sendInit)
            } else {
              self.sendAck()
              self.incrSeq()
              return .next(.complete)
            }
          } else {
            self.send_E_packet(message: response ?? "")
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        self.timerCount = 0
        if self.sendIdleNak {
          self.sendNak(seqNum: 0)
        }
        return .yield
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .yield
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // no packet
      } else {
        return .yield
      }
    
    // RECEIVE-INIT -- entry point for non-server RECEIVE command
    //
    case .receiveInit:
      self.seqNum = 0
      self.retryCount = 0
      
      // S packet
      if packet?.matches(seq: 0, type: "S") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_S_packet(packet!)
          self.sendAckWithParams(seqNum: 0)
          self.incrSeq()
          return .next(.receiveFile)
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        self.timerCount = 0
        self.sendNak(seqNum: 0)
        return .bumpRetry()
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .yield
        } else {
          return .checksumFailure(nakSeq: 0)
        }
      // no packet
      } else {
        return .yield
      }
      
    // RECEIVE-FILE -- look for a file header or EOT packet
    case .receiveFile:
      // F packet
      if packet?.matches(seq: self.seqNum, type: "F") == true {
        if packet!.verifyChecksum(chkSize: self.pendingParameters.checksumSize) {
          self.activatePendingParameters()
          let (success, msg) = self.process_F_packet(packet!)
          if success {
            self.sendAck()
            self.incrSeq()
            return .next(.receiveData)
          } else {
            self.send_E_packet(message: msg)
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // X packet
      } else if packet?.matches(seq: self.seqNum, type: "X") == true {
        if packet!.verifyChecksum(chkSize: self.pendingParameters.checksumSize) {
          self.activatePendingParameters()
          let (success, msg) = self.process_X_packet(packet!)
          if success {
            self.sendAck()
            self.incrSeq()
            return .next(.receiveData)
          } else {
            self.send_E_packet(message: msg)
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // B packet
      } else if packet?.matches(seq: self.seqNum, type: "B") == true {
        if packet!.verifyChecksum(chkSize: self.pendingParameters.checksumSize) {
          self.activatePendingParameters()
          self.sendAck()
          return .next(.complete)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // S packet, previous sequence number
      } else if packet?.matches(seq: self.seqNum-1, type: "S") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.sendAckWithParams(seqNum: self.seqNum-1)
          return .bumpRetry()
        } else {
          return .checksumFailure(nakSeq: self.seqNum-1)
        }
      // Z packet, previous sequence number
      } else if packet?.matches(seq: self.seqNum-1, type: "Z") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.sendAck(seqNum: self.seqNum-1)
          return .bumpRetry()
        } else {
          return .checksumFailure(nakSeq: self.seqNum-1)
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        self.timerCount = 0
        self.sendAck(seqNum: self.seqNum)
        return .bumpRetry()
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // RECEIVE-DATA -- receive data up to end of file
    //
    case .receiveData:
      // D packet
      if packet?.matches(seq: self.seqNum, type: "D") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          let (success, msg) = process_D_packet(packet!)
          if success {
            self.sendAck()
            self.incrSeq()
            return .yield
          } else {
            self.send_E_packet(message: msg)
            return .next(.abort)
          }
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // D packet, previous sequence number
      } else if packet?.matches(seq: self.seqNum-1, type: "D") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.sendAck(seqNum: self.seqNum-1)
          return .bumpRetry()
        } else {
          return .checksumFailure(nakSeq: self.seqNum-1)
        }
      // Z packet
      } else if packet?.matches(seq: self.seqNum, type: "Z") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_Z_packet(packet!)
          self.sendAck()
          self.incrSeq()
          return .next(.receiveFile)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // F packet, previous sequence number
      } else if packet?.matches(seq: self.seqNum-1, type: "F") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.sendAck(seqNum: self.seqNum-1)
          return .bumpRetry()
        } else {
          return .checksumFailure(nakSeq: self.seqNum-1)
        }
      // X packet, previous sequence number
      } else if packet?.matches(seq: self.seqNum-1, type: "X") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.sendAck(seqNum: self.seqNum-1)
          return .bumpRetry()
        } else {
          return .checksumFailure(nakSeq: self.seqNum-1)
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        self.timerCount = 0
        self.sendAck(seqNum: self.seqNum-1)
        return .bumpRetry()
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // SEND-INIT -- entry point for the SEND command
    //
    case .sendInit:
      self.seqNum = 0
      self.retryCount = 0
      self.send_S_packet()
      return .next(.sendInitAck)
      
    case .sendInitAck:
      // Y packet
      if packet?.matches(seq: 0, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.processAckWithParams(pkt: packet!)
          self.incrSeq()
          return .next(.openFile)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendInit)
        } else {
          return .checksumFailure()
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        self.timerCount = 0
        return .bumpRetry(.sendInit)
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendInit)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }
      
    // OPEN-FILE -- open file or setup text to send
    //
    // File should already be opened in workspace when we get
    // here (from processing received 'R' packet when remote end
    // is pulling a named file, or because we manually opened it
    // when we are pushing a file), so let's generate some data
    // to send, then go send it.
    case .openFile:
      let data = self.workspace.readFromCurrentInputStream(maxBytes: 10240)
      self.pendingTxChunks = self.encodeDataToChunks(data)
      self.numUserBytesSent = 0
      return .next(.sendFile)
      
    // SEND-FILE -- send file or text header
    //
    case .sendFile:
      if self.workspace.inputType == .file {
        self.send_F_packet()
        return .next(.sendFileAck)
      } else if self.workspace.inputType == .pad {
        self.send_F_packet()
        return .next(.sendFileAck)
      } else if self.workspace.inputType == .message {
        self.send_X_packet()
        return .next(.sendFileAck)
      } else {
        self.protocolErrMsg = "Oops -- internal error"
        return .next(.abort)
      }
      
    case .sendFileAck:
      // Y packet
      if packet?.matches(seq: self.seqNum, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendData)
        } else {
          return .checksumFailure()
        }
      // N packet, next sequence number
      } else if packet?.matches(seq: self.seqNum+1, type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendData)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendFile)
        } else {
          return .checksumFailure()
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        // self.timerCount = 0  (??? TODO: check it's correct not to do this ???)
        return .bumpRetry(.sendFile)
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // SEND-DATA -- send contents of file or textual information
    //
    case .sendData:
      if self.pendingTxChunks.count > 0 {
        self.send_D_packet()
        return .next(.sendDataAck)
      } else if self.workspace.eofCurrentInputStream() {
        return .next(.sendEOF)
      } else {
        let data = self.workspace.readFromCurrentInputStream(maxBytes: 10240)
        if data.count == 0 {
          return .next(.sendEOF)
        } else {
          self.pendingTxChunks = self.encodeDataToChunks(data)
          send_D_packet()
          return .next(.sendDataAck)
        }
      }
      
    case .sendDataAck:
      // Y packet
      if packet?.matches(seq: self.seqNum, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendData)
        } else {
          return .checksumFailure()
        }
      // N packet, next sequence number
      } else if packet?.matches(seq: self.seqNum+1, type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendData)
        } else {
          return .checksumFailure()
        }
      // Y packet, subtype X (??? TODO: will never match this -- will match Y packet above ???)
      } else if packet?.matches(seq: self.seqNum, type: "Y", subtype: "X") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendEOF)
        } else {
          return .checksumFailure()
        }
      // Y packet, subtype Z (??? TODO: will never match this -- will match Y packet above ???)
      } else if packet?.matches(seq: self.seqNum, type: "Y", subtype: "Z") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendEOF)
        } else {
          return .checksumFailure()
        }
        // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendData)
        } else {
          return .checksumFailure()
        }
        // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
        // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        return .bumpRetry(.sendFile)
        // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
        // no packet
      } else {
        return .yield
      }
      
    // SEND-EOF -- send end-of-file indicator
    //
    case .sendEOF:
      send_Z_packet()
      return .next(.sendEOFAck)
      
    case .sendEOFAck:
      // Y packet
      if packet?.matches(seq: self.seqNum, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendBreak)
        } else {
          return .checksumFailure()
        }
      // N packet, next sequence number
      } else if packet?.matches(seq: self.seqNum+1, type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.sendBreak)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendEOF)
        } else {
          return .checksumFailure()
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        return .bumpRetry(.sendEOF)
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // SEND-BREAK -- end of transaction
    //
    case .sendBreak:
      self.send_B_packet()
      return .next(.sendBreakAck)
    
    case .sendBreakAck:
      // Y packet
      if packet?.matches(seq: self.seqNum, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .next(.complete)
        } else {
          return .checksumFailure()
        }
      // N packet, sequence number 0
      } else if packet?.matches(seq: 0, type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .next(.complete)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .next(.sendBreak)
        } else {
          return .checksumFailure()
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        return .next(.sendBreak)
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // SEND-SERVER-INIT -- entry for server commands which expect large response
    //
    case .sendServerInit:
      self.send_I_packet(seqNum: 0)
      return .next(.sendServerInitAck)
      
    case .sendServerInitAck:
      // Y packet, sequence number 0
      if packet?.matches(seq: 0, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.processAckWithParams(pkt: packet!)
          return .next(.sendGenCmd)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendServerInit)
        } else {
          return .checksumFailure()
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        return .bumpRetry(.sendServerInit)
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.currentParameters = KermitProtocolParameters()
          return .next(.sendGenCmd)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // SEND-GEN-CMD -- entry for server commands that expect short response
    //
    case .sendGenCmd:
      // TODO
      return .next(.sendGenCmdAck)

    case .sendGenCmdAck:
      // S packet, sequence number 0
      if packet?.matches(seq: 0, type: "S") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_S_packet(packet!)
          self.sendAckWithParams(seqNum: 0)
          self.incrSeq()
          return .next(.receiveFile)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // X packet, sequence number 1
      } else if packet?.matches(seq: 1, type: "X") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.incrSeq()
          return .next(.receiveData)
        } else {
          return .checksumFailure(nakSeq: 1)
        }
      // Y packet, sequence number 0
      } else if packet?.matches(seq: 0, type: "Y") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          // TODO
          return .next(.complete)
        } else {
          return .checksumFailure()
        }
      // N packet
      } else if packet?.matches(type: "N") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          return .bumpRetry(.sendGenCmd)
        } else {
          return .checksumFailure()
        }
      // E packet
      } else if packet?.matches(type: "E") == true {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.process_E_packet(packet!)
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // timeout expired
      } else if self.timerCount >= hoppiTimeoutPeriod {
        return .bumpRetry(.sendGenCmd)
      // unknown packet
      } else if let _ = packet {
        if packet!.verifyChecksum(chkSize: checksumSize) {
          self.send_E_packet(message: "Packet not understood")
          return .next(.abort)
        } else {
          return .checksumFailure(nakSeq: self.seqNum)
        }
      // no packet
      } else {
        return .yield
      }

    // COMPLETE -- successful completion of transaction
    //
    case .complete:
      self.seqNum = 0
      self.retryCount = 0
      self.currentParameters = KermitProtocolParameters()
      return .next(.receiveServerIdle)
      
    // ABORT -- premature termination of transaction
    //
    case .abort:
      self.workspace.closeCurrentInputStream()
      self.workspace.closeCurrentOutputStream()
      self.seqNum = 0
      self.retryCount = 0
      self.currentParameters = KermitProtocolParameters()
      if tick {
        return .next(.restart)
      } else {
        return .yield
      }
    
    // RESTART -- wait for 1 second then clear transmit and
    //            receive buffers before restarting
    //
    case .restart:
      self.txBuffer.resetBuffer()
      self.rxBuffer.resetBuffer()
      if tick {
        return .next(.restartNow)
      } else {
        return .yield
      }
      
    case .restartNow:
      self.txBuffer.resetBuffer()
      self.rxBuffer.resetBuffer()
      if tick {
        return .next(.receiveServerIdle)
      } else {
        return .yield
      }
    }
    return .yield
  }
  
  // Keep calling runProtocolStep until it requests that we stop.
  // Supply the the given packet and timer-events on the first call only.
  func runProtocol(packet: KermitPacket? = nil, tick: Bool = false) {
    var transition: KermitProtocolTransition = runProtocolStep(packet: packet, tick: tick)
    self.observer?.didRunProtocolStep(state: self.protocolState)
    
    var stop: Bool = false
    while !stop {
      switch transition {
      case .checksumFailure(let nakSeq):
        self.protocolErrMsg = "Bad checksum"
        if let nakSeq = nakSeq {
          self.sendNak(seqNum: nakSeq)
        }
      case .bumpRetry(let state):
        self.retryCount += 1
        if self.retryCount < self.retryLimit {
          if state != nil {
            self.protocolState = state!
          }
        } else {
          self.send_E_packet(message: "Timed out")
          self.protocolState = .abort
        }
      case .next(let state):
        self.protocolState = state
      case .yield:
        stop = true
      }

      if !stop {
        transition = runProtocolStep()
        self.observer?.didRunProtocolStep(state: self.protocolState)
      }
    }
    self.observer?.didRunProtocol(state: self.protocolState, retryCount: self.retryCount)
  }
  
  func pushPacket(_ packet: KermitPacket) {
    self.runProtocol(packet: packet)
  }
  
  func oneSecondTick() {
    self.runProtocol(tick: true)
  }
  
  func sendFile(_ filename: String, transferType: KWTransferType = .unknown) {
    let (success, msg) = self.workspace.openFileForReading(filename, transferType: transferType)
    if success {
      self.protocolState = .sendInit
      self.runProtocol()
    } else {
      self.txErrMsg = msg
      self.protocolState = .abort
      self.runProtocol()
    }
  }
}

