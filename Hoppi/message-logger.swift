//
//  message-logger.swift
//  Hoppi
//
//  Created by Paul Onions on 18/10/2020.
//  Copyright © 2020 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Cocoa

@objc class MessageLogger : NSWindowController {
  override var windowNibName: String! { return "message-logger" }

  @IBOutlet var logTranscriptWidget: NSTextView!
  
  override func windowDidLoad() {
    super.windowDidLoad()
  }

  @IBAction func clearLog(_ sender: NSButton) {
    self.logTranscriptWidget.string = ""
  }

  func appendTxMessage(title: String, message: String) {
    let log = "↩ \(title)\n\(message)\n"
    
    let txtstore = self.logTranscriptWidget.textStorage
    let n = txtstore?.length ?? 0
    txtstore?.replaceCharacters(in: NSRange(location: n, length: 0), with: log)
      
    let m = self.logTranscriptWidget.string.count
    self.logTranscriptWidget.scrollRangeToVisible(NSRange(location: m, length: 0))
    self.logTranscriptWidget.needsDisplay = true
  }
  
  func appendRxMessage(title: String, message: String) {
    let log = "↪ \(title)\n\(message)\n"
    
    let txtstore = self.logTranscriptWidget.textStorage
    let n = txtstore?.length ?? 0
    txtstore?.replaceCharacters(in: NSRange(location: n, length: 0), with: log)
      
    let m = self.logTranscriptWidget.string.count
    self.logTranscriptWidget.scrollRangeToVisible(NSRange(location: m, length: 0))
    self.logTranscriptWidget.needsDisplay = true
  }
}
