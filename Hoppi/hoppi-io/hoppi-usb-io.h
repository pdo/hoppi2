//
// Copyright 2012 - 2020 Paul Onions
//
// Licence: MIT, see LICENCE file in this directory.
//
// Low-level USB I/O for the Hoppi project
//
#include <IOKit/IOKitLib.h>

//
// Function return codes
//
#define SUCCESS 0

// return codes for setup_usb_io()
#define CANNOT_CREATE_IOKIT_MASTER_PORT       (-1)
#define CANNOT_CREATE_MATCHING_DICTIONARY     (-2)
#define CANNOT_ADD_ARRIVAL_NOTIFICATION       (-3)
#define CANNOT_ADD_DEPARTURE_NOTIFICATION     (-4)

// return codes for configure_usb_device()
#define CANNOT_CREATE_PLUGIN_INTERFACE        (-1)
#define CANNOT_CREATE_DEVICE_INTERFACE        (-2)
#define WRONG_VENDOR_ID                       (-3)
#define WRONG_PRODUCT_ID                      (-4)
#define CANNOT_OPEN_DEVICE                    (-5)
#define CANNOT_GET_NUMBER_OF_CONFIGURATIONS   (-6)
#define NO_CONFIGURATIONS_AVAILABLE           (-7)
#define CANNOT_GET_CONFIGURATION_0_DESCRIPTOR (-8)
#define CANNOT_SET_CONFIGURATION              (-9)
#define CANNOT_CREATE_INTERFACE_ITERATOR      (-10)
#define CANNOT_OPEN_INTERFACE                 (-11)
#define CANNOT_GET_NUMBER_OF_ENDPOINTS        (-12)
#define WRONG_NUMBER_OF_ENDPOINTS             (-13)
#define CANNOT_CREATE_ASYNC_EVENT_SOURCE      (-14)
#define CANNOT_EXAMINE_ENDPOINT_1             (-15)
#define CANNOT_EXAMINE_ENDPOINT_2             (-16)
#define NO_IN_ENDPOINT                        (-17)
#define NO_OUT_ENDPOINT                       (-18)
#define NO_WANTED_INTERFACE_FOUND             (-19)

//
// Function prototypes
//
int setup_usb_io (void (*post_configure_callback_fn)(void),
                  void (*post_unconfigure_callback_fn)(void));

void teardown_usb_io(void);

int post_usb_async_read(void *buffer, UInt32 buffer_len, IOAsyncCallback1 callback_fn, void *callback_data);

int post_usb_async_write(void *buffer, UInt32 buffer_len, IOAsyncCallback1 callback_fn, void *callback_data);
