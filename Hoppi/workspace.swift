//
//  workspace.swift
//  Hoppi
//
//  Created by Paul Onions on 01/08/2020.
//  Copyright © 2020 - 2022 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Foundation

enum KWTransferType {
  case unknown
  case text
  case textAsString
  case binary
}

enum KWEndpointType {
  case file
  case pad
  case message
}

enum KWListingStyle {
  case RPL
  case DOS
}

enum KWStringDecodingState {
  case removingHeader
  case decoding
  case decodingEscaped
}

protocol KermitWorkspaceObserver : AnyObject {
  func didOpenFileForWriting(filename: String, transferType: KWTransferType)
  func didOpenPadForWriting(transferType: KWTransferType)
  func didOpenMessageForWriting(title: String, transferType: KWTransferType)
  func didWriteToCurrentOutputStream(data: [UInt8])
  func didReceivePad(text: String)
  func didReceiveMessage(title: String, message: String)
  func didOpenFileForReading(filename: String, transferType: KWTransferType)
  func willOpenPadForReading(transferType: KWTransferType) -> String
  func didOpenMessageForReading(title: String, msg: String, transferType: KWTransferType)
  func didUpdateCurrentFolderPath(path: String)
  func didSetAllowOverwrite(allow: Bool)
  func didSetTransferType(type: KWTransferType)
}

class KermitWorkspace {
  unowned let observer: KermitWorkspaceObserver?
  
  let fm: FileManager = FileManager()
  var strDecState: KWStringDecodingState = .removingHeader
  var inputStore: [UInt8] = []
  var inputType: KWEndpointType = .file
  var inputUrl: URL? = nil            // for files
  var inputHandle: FileHandle? = nil  // for files
  var inputTitle: String = ""         // for messages
  var inputPad: String = ""           // for messages
  var outputType: KWEndpointType = .file
  var outputUrl: URL? = nil            // for files
  var outputHandle: FileHandle? = nil  // for files
  var outputTitle: String = ""         // for messages
  var outputPad: String = ""           // for messages

  var allowOverwrite: Bool = false {
    didSet { self.observer?.didSetAllowOverwrite(allow: self.allowOverwrite)}
  }

  var transferType: KWTransferType = .unknown {
    didSet { self.observer?.didSetTransferType(type: self.transferType)}
  }

  init(observer: KermitWorkspaceObserver? = nil) {
    self.observer = observer
  }
  
  func homeFolderPath() -> String {
    return self.fm.homeDirectoryForCurrentUser.absoluteString
  }

  func currentFolderPath() -> String {
    return self.fm.currentDirectoryPath
  }

  // Return byte array with backslashes and double-quotes escaped
  func encodeAsContentsOfString(_ bytes: [UInt8]) -> [UInt8] {
    let backslash = Character("\\").asciiValue!
    let doublequote = Character("\"").asciiValue!

    var encoded: [UInt8] = []
    for byte in bytes {
      if byte == doublequote {
        encoded.append(backslash)
        encoded.append(doublequote)
      } else if byte == backslash {
        encoded.append(backslash)
        encoded.append(backslash)
      } else {
        encoded.append(byte)
      }
    }
    return encoded
  }
  
  // Return byte array with backslashes and double-quotes unescaped
  func decodeAsContentsOfString(_ bytes: [UInt8]) -> [UInt8] {
    let backslash = Character("\\").asciiValue!
    let doublequote = Character("\"").asciiValue!
    let newline = Character("\n").asciiValue!
    
    var decoded: [UInt8] = []
    for byte in bytes {
      if self.strDecState == .removingHeader {
        if byte == newline {
          self.strDecState = .decoding
        }
      } else if self.strDecState == .decoding {
        if byte == backslash {
          self.strDecState = .decodingEscaped
        } else if byte != doublequote {
          decoded.append(byte)
        }
      } else if self.strDecState == .decodingEscaped {
        if byte == backslash {
          decoded.append(byte)
          self.strDecState = .decoding
        } else if byte == doublequote {
          decoded.append(byte)
          self.strDecState = .decoding
        } else {
          decoded.append(backslash)
          decoded.append(byte)
          self.strDecState = .decoding
        }
      }
    }
    return decoded
  }

  // Open file for writing, returning true for success, false for failure.
  // Also return a string as secondary result: on success return the full
  // path of the file opened, and on failure return an error message.
  func openFileForWriting(_ filepath: String, transferType: KWTransferType) -> (Bool, String) {
    try? self.outputHandle?.close()
    self.transferType = transferType
    self.strDecState = .removingHeader
    self.outputType = .file
    self.outputUrl = URL(fileURLWithPath: filepath)
    self.outputHandle = nil

    var isDirObjC : ObjCBool = false
    let exists = self.fm.fileExists(atPath: self.outputUrl!.path, isDirectory: &isDirObjC)
    let isDir = isDirObjC.boolValue
    
    if exists && isDir {
      return (false, "Cannot overwrite a directory: \(self.outputUrl!.path)")
    } else if exists && !isDir && !self.allowOverwrite {
      return (false, "Cannot overwrite file: \(self.outputUrl!.path)")
    } else if exists && !isDir {
      try? self.fm.removeItem(atPath: self.outputUrl!.path)
    }

    if !self.fm.createFile(atPath: self.outputUrl!.path, contents: nil, attributes: nil) {
      return (false, "Cannot create file: \(self.outputUrl!.path)")
    }
    
    if let handle = FileHandle(forWritingAtPath: self.outputUrl!.path) {
      self.outputHandle = handle
      self.observer?.didOpenFileForWriting(filename: self.outputUrl!.path, transferType: self.transferType)
      return (true, self.outputUrl!.path)
    } else {
      return (false, "Cannot write to file: \(self.outputUrl!.path)")
    }
  }
  
  // Open the transfer pad for writing.
  // Return true and the empty string -- same return types as openFileForWriting().
  func openPadForWriting(transferType: KWTransferType) -> (Bool, String) {
    try? self.outputHandle?.close()
    self.transferType = transferType
    self.strDecState = .removingHeader
    self.outputType = .pad
    self.outputTitle = ""
    self.outputPad = ""
    self.observer?.didOpenPadForWriting(transferType: self.transferType)
    return (true, "")
  }
  
  // Open a message for writing.
  // Return true and the title -- same return types as openFileForWriting().
  func openMessageForWriting(title: String, transferType: KWTransferType) -> (Bool, String) {
    try? self.outputHandle?.close()
    self.transferType = transferType
    self.strDecState = .removingHeader
    self.outputType = .message
    self.outputTitle = title
    self.outputPad = ""
    self.observer?.didOpenMessageForWriting(title: self.outputTitle, transferType: self.transferType)
    return (true, title)
  }
  
  // Write the given vector of byte values to the current output stream.
  // Return true for success, false for failure.  Also return a string as
  // secondary result: on success return the path of the file to which we
  // are writing, or an empty string if we are writing a message, and on
  // failure return an error message.
  func writeToCurrentOutputStream(data: [UInt8]) -> (Bool, String) {
    // Determine transferType if necessary
    if self.transferType == .unknown {
      let hphp48: [UInt8] = "HPHP48".map{$0.asciiValue!}
      let hphp49: [UInt8] = "HPHP49".map{$0.asciiValue!}
      if data.count >= 6 && (Array(data[0..<6]) == hphp48 || Array(data[0..<6]) == hphp49) {
        self.transferType = .binary
      } else {
        self.transferType = .text
      }
    }

    // Write output, taking note of outputType and transferType
    switch outputType {
    case .file:
      switch self.transferType {
      case .text:
        let text = String(data.map(hpCodeToUnicodeChar))
        let odata = text.data(using: .utf8)!
        self.outputHandle?.write(odata)
      case .textAsString:
        let text = String(self.decodeAsContentsOfString(data).map(hpCodeToUnicodeChar))
        let odata = text.data(using: .utf8)!
        self.outputHandle?.write(odata)
      case .binary:
        let odata = Data(data)
        self.outputHandle?.write(odata)
      case .unknown:
        break
      }
    case .pad:
      switch self.transferType {
      case .text:
        let text = String(data.map(hpCodeToUnicodeChar))
        self.outputPad.append(contentsOf: text)
      case .textAsString:
        let text = String(self.decodeAsContentsOfString(data).map(hpCodeToUnicodeChar))
        self.outputPad.append(contentsOf: text)
      case .binary:
        return (false, "Cannot accept a binary pad transfer.")
      case .unknown:
        break
      }
    case .message:
      switch self.transferType {
      case .text:
        let text = String(data.map(hpCodeToUnicodeChar))
        self.outputPad.append(contentsOf: text)
      case .textAsString:
        let text = String(self.decodeAsContentsOfString(data).map(hpCodeToUnicodeChar))
        self.outputPad.append(contentsOf: text)
      case .binary:
        return (false, "Cannot accept a binary message.")
      case .unknown:
        break
      }
    }
    self.observer?.didWriteToCurrentOutputStream(data: data)
    return (true, self.outputUrl?.path ?? "")
  }
  
  // Close the current output stream
  func closeCurrentOutputStream() {
    switch self.outputType {
    case .pad:
      self.observer?.didReceivePad(text: self.outputPad)
    case .message:
      self.observer?.didReceiveMessage(title: self.outputTitle, message: self.outputPad)
    default:
      break
    }
    try? self.outputHandle?.close()
    self.outputHandle = nil
    self.transferType = .unknown
  }
  
  // Open file for reading, returning true for success, false for failure.
  // Also return a string as secondary result: on success return the full
  // path of the file opened, and on failure return an error message.
  func openFileForReading(_ filepath: String, transferType: KWTransferType) -> (Bool, String) {
    try? self.inputHandle?.close()
    self.inputType = .file
    self.inputUrl = URL(fileURLWithPath: filepath)
    self.inputHandle = nil

    var isDirObjC : ObjCBool = false
    let exists = self.fm.fileExists(atPath: self.inputUrl!.path, isDirectory: &isDirObjC)
    let isDir = isDirObjC.boolValue

    if exists && isDir {
      return (false, "Cannot read from a directory: \(self.inputUrl!.path)")
    } else if !exists {
      return (false, "Cannot find file: \(self.inputUrl!.path)")
    } else {
      if let handle = FileHandle(forReadingAtPath: self.inputUrl!.path) {
        self.inputHandle = handle
      }
    }

    // Return error message if cannot open file
    if self.inputHandle == nil {
      return (false, "Cannot open file: \(self.inputUrl!.path)")
    }
    
    // Read the whole file into our input store
    self.inputStore = Array(self.inputHandle!.availableData)

    // Examine the file type, text or binary
    var fileType: KWTransferType = .text  // default
    if self.inputStore.count >= 6 {
      let prefix = Array(self.inputStore[0..<6])
      let hphp48 = "HPHP48".map{$0.asciiValue!}
      let hphp49 = "HPHP49".map{$0.asciiValue!}
      if prefix == hphp48 || prefix == hphp49 {
        fileType = .binary
      }
    }
      
    // Figure out what transfer type to actually use
    if fileType == .text && (transferType == .text || transferType == .unknown) {
      self.transferType = .text
    } else if fileType == .text && transferType == .textAsString {
      self.transferType = .textAsString
    } else if fileType == .binary && (transferType == .binary || transferType == .unknown) {
      self.transferType = .binary
    } else if fileType == .binary && (transferType == .text || transferType == .textAsString) {
      self.transferType = .unknown
      return (false, "Cannot send BINARY file as text or string")
    } else {
      self.transferType = .unknown
      return (false, "Cannot determine transfer type")
    }

    // When transfer type is .text or .textAsString then assume file contents
    // are UTF8 unicode and try to translate them to calculator character codes.
    // If this fails then assume file contents are encoded as calculator
    // character codes and send them as is.
    if self.transferType == .text || self.transferType == .textAsString {
      if let utf8Contents = String(bytes: self.inputStore, encoding: .utf8) {
        self.inputStore = utf8Contents.map{unicodeCharToHpCode($0)}
      }
    }
    
    // When transfer type is .textAsString then transform the stored data
    // appropriately, i.e. escaping backslashes and double-quotes.
    if self.transferType == .textAsString {
      let stringPrefix = "%%HP: T(0)A(R)F(.);\n\"".map{$0.asciiValue!}
      let stringContents = encodeAsContentsOfString(self.inputStore)
      let stringSuffix = "\"".map{$0.asciiValue!}
      self.inputStore = []
      self.inputStore.append(contentsOf: stringPrefix)
      self.inputStore.append(contentsOf: stringContents)
      self.inputStore.append(contentsOf: stringSuffix)
    }

    // Return success
    self.observer?.didOpenFileForReading(filename: self.inputUrl!.lastPathComponent, transferType: self.transferType)
    return (true, self.inputUrl!.path)
  }
  
  // Open the transfer pad for reading.
  // Return true and a string -- same return types as openFileForReading().
  func openPadForReading(transferType: KWTransferType) -> (Bool, String) {
    try? self.inputHandle?.close()
    self.transferType = transferType
    self.inputType = .pad
    self.inputTitle = "Transfer Pad"
    self.inputPad = self.observer?.willOpenPadForReading(transferType: self.transferType) ?? ""
    self.inputStore = self.inputPad.map{unicodeCharToHpCode($0)}
    return (true, "Transfer Pad")
  }
  
  // Open message for reading, created from the given title and contents
  func openMessageForReading(title: String, msg: String, transferType: KWTransferType) {
    try? self.inputHandle?.close()
    self.transferType = transferType
    self.inputType = .message
    self.inputTitle = title
    self.inputPad = msg
    self.inputStore = msg.map{unicodeCharToHpCode($0)}
    self.observer?.didOpenMessageForReading(title: self.inputTitle, msg: self.inputPad, transferType: self.transferType)
  }
  
  // Read up to maxBytes from the current input stream.
  // Return an array of byte values no longer than maxBytes,
  // empty only if the end of file has been reached.
  func readFromCurrentInputStream(maxBytes: Int) -> [UInt8] {
    var bytes: [UInt8] = []
    
    if self.inputStore.count > 0 {
      let numBytes = min(maxBytes, self.inputStore.count)
      bytes.append(contentsOf: self.inputStore[0..<numBytes])
      self.inputStore.removeSubrange(0..<numBytes)
    }
    
    return bytes
  }
  
  // Return true if the end of the input stream has been reached
  func eofCurrentInputStream() -> Bool {
    return self.inputStore.isEmpty
  }
  
  // Close the current input stream
  func closeCurrentInputStream() {
    try? self.inputHandle?.close()
    self.inputHandle = nil
    self.inputStore = []
    self.transferType = .unknown
  }
  
  // Update the current folder path according to that requested.
  // Return true for success, false for failure.  Also return a string as
  // secondary value, either the name of the path when successful, or an
  // error message when failed.
  func updateCurrentFolderPath(_ path: String) -> (Bool, String) {
    if self.fm.changeCurrentDirectoryPath(path) {
      let newpath = self.currentFolderPath()
      self.observer?.didUpdateCurrentFolderPath(path: newpath)
      return (true, newpath)
    } else {
      return (false, "Invalid directory: \(path)")
    }
  }
  
  // Parse requested path from calculator and return the actual path to use
  func parsePath(_ path: String) -> String {
    var newpath = path
    // strip any enclosing [ ]
    if path.first == "[" && path.last == "]" {
      newpath = String(path.dropFirst().dropLast())
    }
    // undosify it
    newpath = undosifyPath(newpath)
    // absolute, or relative to the current directory
    if newpath.first == "/" {
      return newpath
    } else {
      return self.currentFolderPath() + "/" + newpath
    }
  }

  // Return a string representing PATH in a DOS-like format
  func dosifyPath(_ path: String) -> String {
    let drive = path.first == "/" ? "H:" : ""
    return drive + path.map{$0 == "/" ? "\\" : $0}
  }

  // Unmangle a `dosified' path string, returning a string
  func undosifyPath(_ path: String) -> String {
    var newpath = path
    if path.count >= 2 {
      let c2 = path.dropFirst().first
      if c2 == ":" {
        newpath = String(path.dropFirst().dropFirst())
      }
    }
    return String(newpath.map{$0 == "\\" ? "/" : $0})
  }

  // List folder for reading, returning a string listing for success, nil for failure.
  // If style is .DOS then list folder enough like 'DIR /W' on DOS so that an HP48
  // can interpret it.  Also return the path of the folder listed as secondary result."
  func listFolderForReading(_ path: String, style: KWListingStyle) -> (String?, String?) {
    let keys = [URLResourceKey.isHiddenKey, URLResourceKey.isDirectoryKey, URLResourceKey.isRegularFileKey]
    let contents = try? self.fm.contentsOfDirectory(at: URL(fileURLWithPath: path, isDirectory: true), includingPropertiesForKeys: keys)
    if contents == nil {
      return (nil, nil)
    } else {
      var files: [String] = []
      var dirs: [String] = []
      for item in contents! {
        let rvals = try? item.resourceValues(forKeys: Set(keys))
        if rvals?.isRegularFile == true && rvals?.isHidden == false {
          files.append(item.lastPathComponent)
        } else if rvals?.isDirectory == true && rvals?.isHidden == false {
          dirs.append(item.lastPathComponent)
        }
      }
      let chosenPath = self.parsePath(path)
      var listing = ""
      switch style {
      case .DOS:
        listing += "Directory of \(dosifyPath(chosenPath))\n[..] "
        for dir in dirs {
          listing += "[\(dir)] "
        }
        for file in files {
          listing += "\(file) "
        }
      case .RPL:
        listing += "\"\(chosenPath)\" "
        listing += "{"
        for dir in dirs {
          listing += "\"\(dir)\" "
        }
        listing += "} {"
        for file in files {
          listing += "\"\(file)\" "
        }
        listing += "}"
      }
      return (listing, chosenPath)
    }
  }
}
