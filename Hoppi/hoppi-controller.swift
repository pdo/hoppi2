//
//  hoppi-controller.swift
//  Hoppi
//
//  Created by Paul Onions on 22/08/2020.
//  Copyright © 2020 - 2022 Paul Onions.
//  Licence: MIT, see LICENCE file in this directory.
//

import Cocoa
import ORSSerial

struct HoppiDefaults {
  static let allowOverwrite = "Hoppi2AllowOverwrite"
  static let currentPath = "Hoppi2CurrentPath"
  static let connectionType = "Hoppi2ConnectionType"
  static let serialDevice = "Hoppi2DevicePath"
}

enum HoppiConnectionType: Int {
  case usb = 0
  case serial = 1
}

@objc class HoppiController : NSObject, ORSSerialPortDelegate, KermitProtocolObserver, KermitWorkspaceObserver {
  @IBOutlet var showPacketLoggerWidget: NSMenuItem!
  @IBOutlet var showMessageLoggerWidget: NSMenuItem!
  @IBOutlet var showGrobViewerWidget: NSMenuItem!
  @IBOutlet var showTransferPadWidget: NSMenuItem!
  @IBOutlet var receiveFileAsStringWidget: NSMenuItem!

  @IBOutlet var windowWidget: NSWindow!
  @IBOutlet var currentPathWidget: NSPathControl!
  @IBOutlet var allowOverwriteWidget: NSButton!
  @IBOutlet var devicePathWidget: NSComboBox!
  @IBOutlet var connectionStatusWidget: NSTextField!
  @IBOutlet var connectionTypeWidget: NSSegmentedControl!
  @IBOutlet var kermitStatusWidget: NSTextField!
  @IBOutlet var kermitResetWidget: NSButton!
  @IBOutlet var messageAreaWidget: NSTextField!
  @IBOutlet var retryCountWidget: NSTextField!
  
  var packetLogger: PacketLogger?
  var messageLogger: MessageLogger?
  var grobViewer: GrobViewer?
  var transferPad: TransferPad?
  
  var kwspace: KermitWorkspace?
  var kpc: KermitProtocolController?
  
  @objc var serman: ORSSerialPortManager?
  @objc var serobs: NSKeyValueObservation?
  
  @objc dynamic var serport: ORSSerialPort? {
    didSet {
      oldValue?.close()
      oldValue?.delegate = nil
      serport?.delegate = self
    }
  }

  var oneSecondTimer: Timer?
  
  var folderChooser: NSOpenPanel?
  var fileChooser: NSOpenPanel?
  
  var ioMsg: String = ""
  var ioName: String = ""
  var ioBytes: Int = 0

  // Initialisation when Hoppi application starts
  //
  override func awakeFromNib() {
    // Create the Kermit workspace object
    self.kwspace = KermitWorkspace(observer: self)
    
    // Create the Kermit protocol controller
    self.kpc = KermitProtocolController(workspace: self.kwspace!, observer: self)
    
    // Create the serial port manager
    self.serman = ORSSerialPortManager.shared()

    // Create the folder-chooser panel
    self.folderChooser = NSOpenPanel()
    self.folderChooser!.canChooseDirectories = true
    self.folderChooser!.canChooseFiles = false
    self.folderChooser!.title = "Choose Folder"
    self.folderChooser!.prompt = "Choose"
    
    // Create the file-chooser panel
    self.fileChooser = NSOpenPanel()
    self.fileChooser!.canChooseDirectories = false
    self.fileChooser!.canChooseFiles = true
    self.fileChooser!.title = "Choose File to Send"
    self.fileChooser!.prompt = "Send"

    // Default directory on first launch
    let homedir = FileManager().homeDirectoryForCurrentUser
    let defdir = homedir.appendingPathComponent("Downloads", isDirectory: true)

    // Register user defaults
    let ud = UserDefaults.standard
    ud.register(defaults: [HoppiDefaults.allowOverwrite: false])
    ud.register(defaults: [HoppiDefaults.currentPath: defdir])
    ud.register(defaults: [HoppiDefaults.connectionType: HoppiConnectionType.serial.rawValue])
    ud.register(defaults: [HoppiDefaults.serialDevice: ""])
    
    // Apply user defaults
    let allowOverwrite = ud.bool(forKey: HoppiDefaults.allowOverwrite)
    self.allowOverwriteWidget.state = allowOverwrite ? .on : .off
    self.kwspace!.allowOverwrite = ud.bool(forKey: HoppiDefaults.allowOverwrite)
    
    let currentPath = ud.url(forKey: HoppiDefaults.currentPath)
    self.currentPathWidget.url = currentPath
    _ = self.kwspace!.updateCurrentFolderPath(currentPath?.path ?? "")
    
    let connectionType = ud.integer(forKey: HoppiDefaults.connectionType)
    self.connectionTypeWidget.selectedSegment = connectionType

    if connectionType == HoppiConnectionType.usb.rawValue {
      setup_usb_io(usbPostConfigureCallback, usbPostUnconfigureCallback)
    }

    if connectionType == HoppiConnectionType.serial.rawValue {
      self.devicePathWidget.isEnabled = true
    }
    
    // Populate the initial serial ports list
    let ports = self.serman!.availablePorts
    self.devicePathWidget.addItems(withObjectValues: ports.map{$0.path})

    // Setup an observer for the serial port manager's available ports list
    let opts: NSKeyValueObservingOptions = [.new]
    self.serobs = self.serman!.observe(\.availablePorts, options: opts) {
      obj, chg in
      self.devicePathWidget.removeAllItems()
      self.devicePathWidget.addItems(withObjectValues: chg.newValue?.map{$0.path} ?? [])
    }

    // Setup observers for the serial device path widget
    NotificationCenter.default.addObserver(self,
      selector: #selector(serialDeviceSelectionChanged),
      name: NSComboBox.selectionDidChangeNotification,
      object: self.devicePathWidget)

    NotificationCenter.default.addObserver(self,
      selector: #selector(serialDeviceTextChanged),
      name: NSControl.textDidChangeNotification,
      object: self.devicePathWidget)

    // Setup an observer to react to the main window being closed
    NotificationCenter.default.addObserver(self,
      selector: #selector(mainWindowWillClose),
      name: NSWindow.willCloseNotification,
      object: self.windowWidget)

    // Start a one-second timer
    self.oneSecondTimer = Timer.scheduledTimer(timeInterval: 1.0,
      target: self, selector: #selector(oneSecondTick(_:)),
      userInfo: nil, repeats: true)
    
    // Set the global 'hoppi' variable to allow the USB callback functions to find us
    hoppi = self
  }

  // Hoppi application termination
  //
  @objc func mainWindowWillClose(_ n: Notification) {
    NSApp.terminate(self)
  }

  // File menu actions
  //
  @IBAction func sendFile(_ sender: NSMenuItem) {
    self.fileChooser?.directoryURL = UserDefaults.standard.url(forKey: HoppiDefaults.currentPath)
    self.fileChooser?.title = "Choose File to Send"
    self.fileChooser?.prompt = "Send"
    self.fileChooser?.beginSheetModal(for: self.windowWidget, completionHandler: self.sendFileCompletion)
  }
  
  func sendFileCompletion(_ response: NSApplication.ModalResponse) {
    if response == .OK {
      if let path = self.fileChooser?.url?.path {
        self.kpc?.sendFile(path)
      }
    }
  }

  @IBAction func sendFileAsString(_ sender: NSMenuItem) {
    self.fileChooser?.directoryURL = UserDefaults.standard.url(forKey: HoppiDefaults.currentPath)
    self.fileChooser?.title = "Choose File to Send"
    self.fileChooser?.prompt = "Send"
    self.fileChooser?.beginSheetModal(for: self.windowWidget, completionHandler: self.sendFileAsStringCompletion)
  }

  func sendFileAsStringCompletion(_ response: NSApplication.ModalResponse) {
    if response == .OK {
      if let path = self.fileChooser?.url?.path {
        self.kpc?.sendFile(path, transferType: .textAsString)
      }
    }
  }

 @IBAction func receiveFileAsString(_ sender: NSMenuItem) {
    if self.kwspace?.transferType == .textAsString {
      self.kwspace?.transferType = .unknown  // turn it off
    } else {
      self.kwspace?.transferType = .textAsString  // turn it on
    }
  }
  
  @IBAction func revealInFinder(_ sender: NSMenuItem) {
    NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: UserDefaults.standard.url(forKey: HoppiDefaults.currentPath)?.path ?? "")
  }
  
  // Help menu actions
  //
  @IBAction func openHoppiUserGuide(_ sender: NSMenuItem) {
    let url = Bundle.main.url(forResource: "Hoppi-User-Guide", withExtension: "pdf")
    if let path = url?.path {
      NSWorkspace.shared.openFile(path)
    }
  }
  
  // Packet logger window management
  //
  @IBAction func togglePacketLogger(_ sender: NSMenuItem) {
    if self.packetLogger == nil {
      self.packetLogger = PacketLogger()
      NotificationCenter.default.addObserver(self,
        selector: #selector(packetLoggerWillClose),
        name: NSWindow.willCloseNotification,
        object: self.packetLogger!.window)
    }
    
    if self.packetLogger!.window!.isVisible {
      self.packetLogger!.window!.setIsVisible(false)
      sender.title = "Show Packet Logger"
    } else {
      self.packetLogger!.window!.setIsVisible(true)
      sender.title = "Hide Packet Logger"
    }
  }
  
  @objc func packetLoggerWillClose(_ n: Notification) {
    self.showPacketLoggerWidget.title = "Show Packet Logger"
  }
  
  // Message logger window management
  //
  @IBAction func toggleMessageLogger(_ sender: NSMenuItem) {
    if self.messageLogger == nil {
      self.messageLogger = MessageLogger()
      NotificationCenter.default.addObserver(self,
        selector: #selector(messageLoggerWillClose),
        name: NSWindow.willCloseNotification,
        object: self.messageLogger!.window)
    }
    
    if self.messageLogger!.window!.isVisible {
      self.messageLogger!.window!.setIsVisible(false)
      sender.title = "Show Message Logger"
    } else {
      self.messageLogger!.window!.setIsVisible(true)
      sender.title = "Hide Message Logger"
    }
  }
  
  @objc func messageLoggerWillClose(_ n: Notification) {
    self.showMessageLoggerWidget.title = "Show Message Logger"
  }
  
  // GROB viewer window management
  //
  func ensureGrobViewer() {
    if self.grobViewer == nil {
      self.grobViewer = GrobViewer()
      NotificationCenter.default.addObserver(self,
        selector: #selector(grobViewerWillClose),
        name: NSWindow.willCloseNotification,
        object: self.grobViewer!.window)
    }
  }
  
  @objc func grobViewerWillClose(_ n: Notification) {
    self.showGrobViewerWidget.title = "Show GROB Viewer"
  }
  
  func showGrobViewer() {
    self.ensureGrobViewer()
    
    if !self.grobViewer!.window!.isVisible {
      self.grobViewer!.window!.setIsVisible(true)
      self.showGrobViewerWidget.title = "Hide GROB Viewer"
    }
  }
  
  @IBAction func toggleGrobViewer(_ sender: NSMenuItem) {
    self.ensureGrobViewer()
    
    if self.grobViewer!.window!.isVisible {
      self.grobViewer!.window!.setIsVisible(false)
      self.showGrobViewerWidget.title = "Show GROB Viewer"
    } else {
      self.grobViewer!.window!.setIsVisible(true)
      self.showGrobViewerWidget.title = "Hide GROB Viewer"
    }
  }
  
  // Transfer Pad window management
  //
  func ensureTransferPad() {
    if self.transferPad == nil {
      self.transferPad = TransferPad()
      NotificationCenter.default.addObserver(self,
        selector: #selector(transferPadWillClose),
        name: NSWindow.willCloseNotification,
        object: self.transferPad!.window)
    }
  }
  
  @objc func transferPadWillClose(_ n: Notification) {
    self.showTransferPadWidget.title = "Show Transfer Pad"
  }
  
  func showTransferPad() {
    self.ensureTransferPad()

    if !self.transferPad!.window!.isVisible {
      self.transferPad!.window!.setIsVisible(true)
      self.showTransferPadWidget.title = "Hide Transfer Pad"
    }
  }

  @IBAction func toggleTransferPad(_ sender: NSMenuItem) {
    self.ensureTransferPad()
    
    if self.transferPad!.window!.isVisible {
      self.transferPad!.window!.setIsVisible(false)
      self.showTransferPadWidget.title = "Show Transfer Pad"
    } else {
      self.transferPad!.window!.setIsVisible(true)
      self.showTransferPadWidget.title = "Hide Transfer Pad"
    }
  }
  
  // Hoppi action methods
  //
  @IBAction func allowOverwriteChanged(_ sender: NSButton) {
    let allowed = sender.state == .on
    UserDefaults.standard.set(allowed, forKey:HoppiDefaults.allowOverwrite)
    self.kwspace?.allowOverwrite = allowed
  }
  
  @IBAction func chooseFolder(_ sender: NSPathControl) {
    self.folderChooser?.directoryURL = UserDefaults.standard.url(forKey: HoppiDefaults.currentPath)
    self.folderChooser?.beginSheetModal(for: self.windowWidget, completionHandler: self.chooseFolderCompletion)
  }

  func chooseFolderCompletion(_ response: NSApplication.ModalResponse) {
    if response == .OK {
      let url = self.folderChooser?.urls[0]
      self.currentPathWidget?.url = url
      UserDefaults.standard.set(url, forKey: HoppiDefaults.currentPath)
      _ = self.kwspace?.updateCurrentFolderPath(url?.path ?? "")
    }
  }

  @IBAction func chooseConnectionType(_ sender: NSSegmentedControl) {
    let usb = HoppiConnectionType.usb.rawValue
    let serial = HoppiConnectionType.serial.rawValue
    if self.connectionTypeWidget?.selectedSegment == usb {
      self.devicePathWidget.isEnabled = false
      self.closeSerialPort()
      UserDefaults.standard.set(usb, forKey: HoppiDefaults.connectionType)
      setup_usb_io(usbPostConfigureCallback, usbPostUnconfigureCallback)
    } else if self.connectionTypeWidget?.selectedSegment == serial {
      teardown_usb_io()
      UserDefaults.standard.set(serial, forKey: HoppiDefaults.connectionType)
      self.devicePathWidget.isEnabled = true
      if let path = self.devicePathWidget.objectValue as? String {
        self.switchToSerialDevice(path: path)
      }
    }
  }
  
  @IBAction func resetKermit(_ sender: NSButton) {
    if let kpc = self.kpc {
      kpc.resetProtocol()
      self.reportKermitStatus(state: kpc.protocolState, retryCount: kpc.retryCount, message: "")
    }
  }
  
  @objc func oneSecondTick(_ sender: NSObject) {
    self.kpc?.oneSecondTick()
    while let byte = self.kpc?.txBuffer.popByte() {
      let data = Data([byte])
      self.serport?.send(data)
    }
  }
  
  // USB port management
  //
  func invokeUsbAsyncRead() {
    let buf = UnsafeMutablePointer<Int8>.allocate(capacity: 128)
    post_usb_async_read(buf, 128, usbAsyncReadCallback, buf)
  }
  
  func invokeUsbAsyncWrite(bytes: [UInt8]) {
    let buf = UnsafeMutableRawPointer.allocate(byteCount: 128, alignment: 1)
    let datalen = min(bytes.count, 128)
    for n in 0..<datalen {
      buf.assumingMemoryBound(to: UInt8.self)[n] = bytes[n]
    }
    post_usb_async_write(buf, UInt32(datalen), usbAsyncWriteCallback, buf)
  }
  
  // Serial port management
  //
  @objc func serialDeviceSelectionChanged(_ n: Notification) {
    if let path = self.devicePathWidget.objectValueOfSelectedItem as? String {
      self.switchToSerialDevice(path: path)
    }
  }

  @objc func serialDeviceTextChanged(_ n: Notification) {
    if let path = self.devicePathWidget.objectValue as? String {
      self.switchToSerialDevice(path: path)
    }
  }

  func switchToSerialDevice(path: String) {
    self.closeSerialPort()
    if UserDefaults.standard.integer(forKey: HoppiDefaults.connectionType) == HoppiConnectionType.serial.rawValue {
      self.openSerialPort(devpath: path)
    }
  }
  
  func openSerialPort(devpath: String) {
    // Close the old port if still present
    self.serport?.close()
    //self.serport?.delegate = nil
    self.serport = nil
    
    // Open the new port
    self.serport = ORSSerialPort(path: devpath)
    //self.serport?.delegate = self
    self.serport?.baudRate = 9600
    self.serport?.parity = .none
    self.serport?.numberOfDataBits = 8
    self.serport?.numberOfStopBits = 1
    self.serport?.usesRTSCTSFlowControl = false
    self.serport?.usesDTRDSRFlowControl = false
    self.serport?.usesDCDOutputFlowControl = false
    self.serport?.open()
    
    if let _ = self.serport?.isOpen {
      UserDefaults.standard.set(devpath, forKey: HoppiDefaults.serialDevice)
      self.reportConnectionStatus("Serial device open")
    }
  }
  
  func closeSerialPort() {
    if let serport = self.serport {
      serport.close()
      serport.delegate = nil
      self.serport = nil
      self.reportConnectionStatus("Serial device closed")
    }
  }

  @objc func serialPortWasRemovedFromSystem(_ serialPort: ORSSerialPort) {
    if serialPort == self.serport {
      self.closeSerialPort()
    }
  }

  //@objc func serialPortWasOpened(_ serialPort: ORSSerialPort) {
  //  NSLog("serialPortWasOpened: \(serialPort.name)")
  //}
    
  //@objc func serialPortWasClosed(_ serialPort: ORSSerialPort) {
  //  NSLog("serialPortWasClosed: \(serialPort.name)")
  //}

  @objc func serialPort(_ serialPort: ORSSerialPort, didReceive data: Data) {
    let bytes = Array(data)
    self.kpc?.rxBuffer.push(bytes: bytes)
    while let pkt = self.kpc?.rxBuffer.popPacket() {
      self.kpc?.pushPacket(pkt)
    }
    while let byte = self.kpc?.txBuffer.popByte() {
      let data = Data([byte])
      self.serport?.send(data)
    }
  }
  
  //@objc func serialPort(_ serialPort: ORSSerialPort, didEncounterError error: Error) {
  //  NSLog("serialPort:didEncounterError: " + error.localizedDescription)
  //}
  
  // Kermit protocol observing
  //
  func didRunProtocol(state: KermitProtocolState, retryCount: Int) {
    self.reportKermitStatus(state: state, retryCount: retryCount)
  }
  
  func didRunProtocolStep(state: KermitProtocolState) {
    if state == .complete {
      self.reportKermitStatus(message: "\(self.ioMsg) \(self.ioName)\nBytes: \(self.ioBytes)\nDone")
    } else if state == .abort {
      self.reportKermitStatus(message: "\(self.ioMsg) \(self.ioName)\nBytes: \(self.ioBytes)\nABORTED")
    }
  }
  
  func didSetTxErrMsg(msg: String) {
    self.ioMsg = "ERROR: \(msg)"
    self.reportKermitStatus(message: "\(self.ioMsg)\n")
  }
  
  func didSetRxErrMsg(msg: String) {
    self.ioMsg = "Received ERROR message: \(msg)"
    self.reportKermitStatus(message: "\(self.ioMsg)\n")
  }
  
  func didProcessSPacket() {
    self.ioMsg = ""
    self.ioName = ""
    self.ioBytes = 0
    self.reportKermitStatus(message: "")
  }
  
  func didProcessIPacket() {
    self.ioMsg = ""
    self.ioName = ""
    self.ioBytes = 0
    self.reportKermitStatus(message: "")
  }
  
  func didSendDPacket(numUserBytesSent: Int) {
    self.ioBytes = numUserBytesSent
    self.reportKermitStatus(message: "\(self.ioMsg) \(self.ioName)\nBytes: \(self.ioBytes)")
  }
  
  func didPushPacket(pkt: KermitPacket) {
    let chkSize = self.kpc!.currentParameters.checksumSize
    self.reportTxPacket(seq: pkt.seq, type: pkt.type, data: pkt.data(chkSize: chkSize), chksum: pkt.chksum(chkSize: chkSize))
  }
  
  func didPopPacket(pkt: KermitPacket) {
    let chkSize = self.kpc!.currentParameters.checksumSize
    self.reportRxPacket(seq: pkt.seq, type: pkt.type, data: pkt.data(chkSize: chkSize), chksum: pkt.chksum(chkSize: chkSize))
  }
  
  // Kermit workspace observing
  //
  func didOpenFileForWriting(filename: String, transferType: KWTransferType) {
    if transferType == .textAsString {
      self.ioMsg = "Receiving file as string:"
    } else {
      self.ioMsg = "Receiving file:"
    }
    self.ioName = filename
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg) \(ioName)")
  }
  
  func didOpenPadForWriting(transferType: KWTransferType) {
    if transferType == .textAsString {
      self.ioMsg = "Receiving transfer pad as string"
    } else {
      self.ioMsg = "Receiving transfer pad"
    }
    self.ioName = ""
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg)")
    self.showTransferPad()
    self.transferPad?.clearPad()
  }
  
  func didOpenMessageForWriting(title: String, transferType: KWTransferType) {
    if transferType == .textAsString {
      self.ioMsg = "Receiving message as string:"
    } else {
      self.ioMsg = "Receiving message:"
    }
    self.ioName = title
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg) \(ioName)")
  }
  
  func didWriteToCurrentOutputStream(data: [UInt8]) {
    self.ioBytes += data.count
    self.reportKermitStatus(message: "\(self.ioMsg) \(self.ioName)\nBytes: \(self.ioBytes)")
  }
  
  func didReceivePad(text: String) {
    self.transferPad?.updateContents(text: text)

    if let (width, height, pixels) = parseGrob(text: text) {
      self.showGrobViewer()
      self.grobViewer?.showGrob(width: width, height: height, pixels: pixels)
    }
  }
  
  func didReceiveMessage(title: String, message: String) {
    self.reportRxMessage(title: title, message: message)
  }
  
  func didOpenFileForReading(filename: String, transferType: KWTransferType) {
    if transferType == .textAsString {
      self.ioMsg = "Sending file as string:"
    } else {
      self.ioMsg = "Sending file:"
    }
    self.ioName = filename
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg) \(ioName)")
  }

  func willOpenPadForReading(transferType: KWTransferType) -> String {
    if transferType == .textAsString {
      self.ioMsg = "Sending transfer pad as string"
    } else {
      self.ioMsg = "Sending transfer pad"
    }
    self.ioName = ""
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg)")
    self.showTransferPad()
    return self.transferPad?.getContents() ?? ""
  }
  
  func didOpenMessageForReading(title: String, msg: String, transferType: KWTransferType) {
    if transferType == .textAsString {
      self.ioMsg = "Sending message as string:"
    } else {
      self.ioMsg = "Sending message:"
    }
    self.ioName = title
    self.ioBytes = 0
    self.reportKermitStatus(message: "\(ioMsg) \(ioName)")
    self.reportTxMessage(title: title, message: msg)
  }

  func didUpdateCurrentFolderPath(path: String) {
    let url = URL(fileURLWithPath: path, isDirectory: true)
    self.currentPathWidget.url = url
    UserDefaults.standard.set(url, forKey: HoppiDefaults.currentPath)
  }
  
  func didSetAllowOverwrite(allow: Bool) {
    self.allowOverwriteWidget.state = allow ? .on : .off
    UserDefaults.standard.set(allow, forKey: HoppiDefaults.allowOverwrite)
  }

  func didSetTransferType(type: KWTransferType) {
    if type == .textAsString {
      self.receiveFileAsStringWidget.state = .on
    } else {
      self.receiveFileAsStringWidget.state = .off
    }
  }
  
  // Connection status reporting
  //
  func clearConnectionStatus() {
    self.connectionStatusWidget.stringValue = ""
  }

  func reportConnectionStatus(_ message: String) {
    self.connectionStatusWidget.stringValue = message
  }

  // Kermit status reporting
  //
  func reportKermitStatus(state: KermitProtocolState? = nil, retryCount: Int? = nil, message: String? = nil) {
    if let state = state {
      var text = ""
      switch state {
      case .receiveServerIdle: text = "Server idle"
      case .receiveInit:       text = "Initializing incoming connection"
      case .receiveFile:       text = "Receiving file name"
      case .receiveData:       text = "Receiving file data"
      case .sendInit:          text = "Initializing outgoing connection"
      case .sendInitAck:       text = "Initializing outgoing connection"
      case .openFile:          text = "Opening outgoing file"
      case .sendFile:          text = "Sending file name"
      case .sendFileAck:       text = "Sending file name"
      case .sendData:          text = "Sending file data"
      case .sendDataAck:       text = "Sending file data"
      case .sendEOF:           text = "Sending end-of-file"
      case .sendEOFAck:        text = "Sending end-of-file"
      case .sendBreak:         text = "Terminating outgoing connection"
      case .sendBreakAck:      text = "Terminating outgoing connection"
      case .sendServerInit:    text = "Initializing server response"
      case .sendServerInitAck: text = "Initializing server response"
      case .sendGenCmd:        text = "Sending server response"
      case .sendGenCmdAck:     text = "Sending server response"
      case .complete:          text = "Transaction complete"
      case .abort:             text = "Transaction aborting"
      case .restart:           text = "Kermit protocol restarting"
      case .restartNow:        text = "Kermit protocol restarting"
      }
      self.kermitStatusWidget.stringValue = text
    }

    if let retryCount = retryCount {
      if retryCount == 0 {
        self.retryCountWidget.stringValue = ""
      } else {
        self.retryCountWidget.stringValue = "Retry \(retryCount)"
      }
    }

    if let message = message {
      self.messageAreaWidget.stringValue = message
    }
  }

  // Kermit packet reporting
  //
  func reportTxPacket(seq: UInt8, type: UInt8, data: [UInt8], chksum: [UInt8]) {
    self.packetLogger?.appendTxPacket(seq: seq, type: type, data: data, chksum: chksum)
  }
  
  func reportRxPacket(seq: UInt8, type: UInt8, data: [UInt8], chksum: [UInt8]) {
    self.packetLogger?.appendRxPacket(seq: seq, type: type, data: data, chksum: chksum)
  }
  
  // Kermit message reporting
  //
  func reportTxMessage(title: String, message: String) {
    self.messageLogger?.appendTxMessage(title: title, message: message)
  }
  
  func reportRxMessage(title: String, message: String) {
    self.messageLogger?.appendRxMessage(title: title, message: message)
  }
}

// USB hoppi-io callbacks from C code
//
var hoppi : HoppiController?

func usbPostConfigureCallback() {
  hoppi?.reportConnectionStatus("Calculator detected")
  hoppi?.invokeUsbAsyncRead()
}

func usbPostUnconfigureCallback() {
  hoppi?.reportConnectionStatus("Calculator off/disconnected")
}

func usbAsyncReadCallback(userData: UnsafeMutableRawPointer?, result: Int32, numBytesWritten: UnsafeMutableRawPointer?) {
  let rxbuf = hoppi?.kpc?.rxBuffer
  let numBytes = Int(bitPattern: numBytesWritten)
  if numBytes > 0 {
    for n in 0..<numBytes {
      if let byte = userData?.assumingMemoryBound(to: UInt8.self)[n] {
        rxbuf?.push(byte: byte)
      }
    }
  }
  userData?.deallocate()
  while let pkt = rxbuf?.popPacket() {
    hoppi?.kpc?.pushPacket(pkt)
  }
  let txbuf = hoppi?.kpc?.txBuffer
  while txbuf?.byteAvailable() ?? false {
    if let bytesAvail = txbuf?.bytesAvailable() {
      if let bytes = txbuf?.popBytes(length: min(bytesAvail, 128)) {
        hoppi?.invokeUsbAsyncWrite(bytes: bytes)
      }
    }
  }
  hoppi?.invokeUsbAsyncRead()
}

func usbAsyncWriteCallback(userData: UnsafeMutableRawPointer?, result: Int32, numBytesWritten: UnsafeMutableRawPointer?) {
  userData?.deallocate()
}

// Utility functions
//
func parseNatural(_ str: Substring) -> (value: Int, remainder: Substring) {
  var value = 0
  var idx = str.startIndex

  // Skip non-digits
  while Int(String(str[idx])) == nil {
    idx = str.index(idx, offsetBy: 1)
  }
  
  // Accumulate digits
  while let d = Int(String(str[idx])) {
    value = 10*value + d
    idx = str.index(idx, offsetBy: 1)
  }
  
  return (value: value, remainder: str[idx...])
}

func parseGrob(text: String) -> (width: Int, height: Int, pixels: [UInt8])? {
  if let rng = text.range(of: "GROB") {
    let (width, rem1) = parseNatural(text[rng.upperBound...])
    let (height, rem2) = parseNatural(rem1)
    let pixelChars = rem2[rem2.index(rem2.startIndex, offsetBy: 1)...]
    
    if 0 < width && width < 1000 && 0 < height && height < 1000 {
      var pixels = Array<UInt8>(repeating: 0, count: height*width)
      let nibblesPerRow = ((width+7)/8) * 2
      var n = 0
      for c in pixelChars {
        let cc = c.asciiValue ?? 0
        let nibble = (cc >= 48 && cc < 58) ? cc - 48 :  // 0 - 9
          (cc >= 65 && cc < 71) ? cc - 55 :             // A - F
          (cc >= 97 && cc < 103) ? cc - 87 : 0          // a - f
        let row = n / nibblesPerRow
        let col = n % nibblesPerRow
        if row < height {
          for j in 0..<4 {
            if 4*col + j < width {
              pixels[row*width + 4*col + j] = (nibble >> j) % 2
            }
          }
        }
        n += 1
      }
      return (width: width, height: height, pixels: pixels)
    }
  }
  return nil  // No valid GROB found
}
